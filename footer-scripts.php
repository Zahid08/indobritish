<script type="text/javascript" src="assets/js/wow.min.js"></script>
<script type="text/javascript">
    wow = new WOW({
        animateClass: 'animated',
        offset: 100,
        callback: function(box) {
            console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
    });
    wow.init();
    //        document.getElementById('slider-animation').onclick = function () {
    //            var section = document.createElement('section');
    //            section.className = 'section--purple wow fadeInDown';
    //            this.parentNode.insertBefore(section, this);
    //        };
</script>
<script type="text/javascript">
    $("#hi").click(function() {
        $(".app-header__logo").toggleClass("app-header__logo-close");
        $(".main-container_close").toggleClass("main-container");
    });
    $(".btn-add").click(function() {
        $(".fa-minus-circle").toggleClass("fa-plus-circle");
    });

    function InfoMessage(title, message) {
        swal(title, message, 'info');
    }

    function SuccessMessage(title, message) {
        swal(title, message, 'success');
    }

    function ErrorMessage(title, message) {
        swal(title, message, 'error');
    }
</script>
<script type="text/javascript" src="assets/js/venom-button.min.js"></script>
<script type="text/javascript" src="assets/js/slick.js"></script>
<script type="text/javascript">
    $('.autoplay').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });
</script>
<script type="text/javascript">
    $('.single-item').slick();
</script>
<script type="text/javascript" src="assets/js/owl.carousel.js"></script>
<script type="text/javascript">
    var sync1 = $(".slider1");
    var sync2 = $(".navigation-thumbs");
    var thumbnailItemClass = '.owl-item';
    var slides = sync1.owlCarousel({
        video: true,
        startPosition: 0,
        items: 1,
        loop: false,
        margin: 0,
        autoplay: false,
        autoplayTimeout: 6000,
        autoplayHoverPause: false,
        nav: false,
        dots: false
    }).on('changed.owl.carousel', syncPosition);

    function syncPosition(el) {
        $owl_slider = $(this).data('owl.carousel');
        var loop = $owl_slider.options.loop;
        if(loop) {
            var count = el.item.count - 1;
            var current = Math.round(el.item.index - (el.item.count / 2) - .5);
            if(current < 0) {
                current = count;
            }
            if(current > count) {
                current = 0;
            }
        } else {
            var current = el.item.index;
        }
        var owl_thumbnail = sync2.data('owl.carousel');
        var itemClass = "." + owl_thumbnail.options.itemClass;
        var thumbnailCurrentItem = sync2.find(itemClass).removeClass("synced").eq(current);
        thumbnailCurrentItem.addClass('synced');
        if(!thumbnailCurrentItem.hasClass('active')) {
            var duration = 600;
            sync2.trigger('to.owl.carousel', [current, duration, true]);
        }
    }
    var thumbs = sync2.owlCarousel({
        startPosition: 1,
        items: 6,
        loop: false,
        margin: 0,
        autoplay: false,
        nav: false,
        dots: false,
        onInitialized: function(e) {
            var thumbnailCurrentItem = $(e.target).find(thumbnailItemClass).eq(this._current);
            thumbnailCurrentItem.addClass('synced');
        },
    }).on('mouseover', thumbnailItemClass, function(e) {
        e.preventDefault();
        var duration = 600;
        var itemIndex = $(e.target).parents(thumbnailItemClass).index();
        sync1.trigger('to.owl.carousel', [itemIndex, duration, true]);
    }).on("changed.owl.carousel", function(el) {
        var number = el.item.index;
        $owl_slider = sync1.data('owl.carousel');
        $owl_slider.to(number, 100, true);
    });
</script>
<script type="text/javascript" src="assets/js/jquery.vticker-min.js"></script>
<script type="text/javascript">
    $('.Mticker').vTicker({
        speed: 1000,
        pause: 3000,
        showItems: 3,
        mousePause: true,
        height: 500,
        direction: 'up'
    });
</script>