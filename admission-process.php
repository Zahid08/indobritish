<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="ms-icon-144x144.php" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/slick.css" />
    <link rel="stylesheet" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/notify-bootstrap.css" />
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css" />
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
    <style type="text/css">
        .table {
            border: 1px solid #ddd;
            font-family: 'Robotoslab';
        }

        th,
        td {
            border: 1px solid #ddd;
        }

        .table thead th {
            background: #085CA6;
            color: #fff;
        }
    </style>
</head>

<body style="background: none">
<form method="post" action="https://indo-british.com/admission-process.aspx" id="form1">
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTY1NDU2MTA1MmRkVCqBYQjkj0lidC5oOGPtg8et4N3Bro3jrbp8hGyw7ho=" /> </div>
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F11100BF" />
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAARYwKw4x1NEXvZ75pFiSBXXt0GqqGmTunmU8BFOEDL5JLt0vKVmDtQYb7KLGs2qWD1/2i7OlKCH0kBh9oGA4EXWZpiu6ytE0dfUQU1kpEFgaeiMf6YjJqvfx1zrKf6ZehE=" /> </div>
    <div>
        <?php include 'header.php';?>
        <div id="myButton"></div>
    </div>
    <div class="page-banner"> <img src="assets/images/sub1.jpg" alt="sub-banner" width="100%" class="sub-banner" />
        <h1 class="h1-banner">
            ADMISSION PROCESS</h1> </div>
    <div class="page">
        <div class="container">
            <div class="m-t-100 m-b-100">
                <p> Indo-british Global School admission process seeks to provide every child with a fair and equal opportunity to develop her ability and potential towards successful completion of education. The admission process shall begin a year in advance. </p>
                <h2 class="subtitle p-t-20">
                    AGE CRITERIA FOR <span>ADMISSION</span></h2>
                <p class="p-t-20"> As Per rules promulgated by the (CBSE / ICSE / IGCSE) the Board Examinations ( secondary level) shall be open to students who have attained the age of 15 years as on 1st April of the year in which they take the examination. </p>
                <div class="p-t-50 wow fadeInUp">
                    <table class="table">
                        <thead>
                        <tr>
                            <th> Class </th>
                            <th> Minimum age on 31st March of the year in which admission is sought </th>
                            <th> Maximum age on 31st March of the year in which admission is sought </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td> I </td>
                            <td> 5 Years </td>
                            <td> 7 Years </td>
                        </tr>
                        <tr>
                            <td> II </td>
                            <td> 6 Years </td>
                            <td> 8 Years </td>
                        </tr>
                        <tr>
                            <td> III </td>
                            <td> 7 Years </td>
                            <td> 9 Years </td>
                        </tr>
                        <tr>
                            <td> III </td>
                            <td> 7 Years </td>
                            <td> 9 Years </td>
                        </tr>
                        <tr>
                            <td> IV </td>
                            <td> 8 Years </td>
                            <td> 10 Years </td>
                        </tr>
                        <tr>
                            <td> V </td>
                            <td> 9 Years </td>
                            <td> 11 Years </td>
                        </tr>
                        <tr>
                            <td> VI </td>
                            <td> 10 Years </td>
                            <td> 12 Years </td>
                        </tr>
                        <tr>
                            <td> VII </td>
                            <td> 11 Years </td>
                            <td> 13 Years </td>
                        </tr>
                        <tr>
                            <td> IX </td>
                            <td> 12 Years </td>
                            <td> 14 Years </td>
                        </tr>
                        <tr>
                            <td> X </td>
                            <td> 13 Years </td>
                            <td> 15 Years </td>
                        </tr>
                        <tr>
                            <td> XI </td>
                            <td> 14 Years </td>
                            <td> 16 Years </td>
                        </tr>
                        <tr>
                            <td> XII </td>
                            <td> 15 Years </td>
                            <td> 17 Years </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="p-t-50 wow rollIn">
                    <h2 class="subtitle">
                        ENTRY CRITERIA FOR <span>ADMISSION</span></h2>
                    <ul class="p-t-20 list-none">
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Admission is to be granted to student on the basis of level rediness to check the fitness into the Indo-Brish Global School System.</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Student viva to be considered only as an interface tool ll class VII. It has no weight in the final selecon. </p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Concept check shall be based on expected key knowledge for every class. </p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Observaon and viva will be conducted in person. Students need to be present for this.</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Student of Indo Brish School will Not have any entrance test assessment in case of internal transfer. </p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> No Admission test ll Class II.</p>
                        </li>
                    </ul>
                </div>
                <table class="table wow fadeInUp">
                    <thead>
                    <tr>
                        <th> Class </th>
                        <th> Assessment Type </th>
                        <th> Assessment composition </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td> Pre-Primary to Std.ll </td>
                        <td> Child Observation + parents interaction </td>
                        <td> 100% </td>
                    </tr>
                    <tr>
                        <td rowspan="3"> Std. III to VII </td>
                        <td> Concept-based assessments on:English - 15% | Hindi -10% | Math-15% Science - 15% | Social Science -15% </td>
                        <td> 70% </td>
                    </tr>
                    <tr>
                        <td> Previous result </td>
                        <td> 30% </td>
                    </tr>
                    <tr>
                        <td> Interaction with assessment on : </td>
                        <td> Mandatory </td>
                    </tr>
                    <tr>
                        <td rowspan="3"> Std. VIII to X </td>
                        <td> Concept - based assessment on :English | Hindi | Math | Science | Social Studies </td>
                        <td> 50% </td>
                    </tr>
                    <tr>
                        <td> Previous result </td>
                        <td> 50% </td>
                    </tr>
                    <tr>
                        <td> Viva </td>
                        <td> 10% </td>
                    </tr>
                    <tr>
                        <td rowspan="2"> Std. VIII to XII (admission to be granted only for transfer case or under special provisions of Government requirements ) </td>
                        <td> Std. X board exam results </td>
                        <td> 50% </td>
                    </tr>
                    <tr>
                        <td> Aptude ( Interest Inventory ) + Subject -Based test </td>
                        <td> 50% </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center"> Admission Priority for std XI is on the basis of merit. Admission to Class XII will be granted subject to availability of seats. </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php include 'footer.php';?>
    </div>
    </div>
    <div class="modal" id="modalPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    <h2 class="subtitle text-left">
                        For Admissions</h2>
                    <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
                </div>
                <div class="m-t-20">
                    <label class="form-label"> Name</label>
                    <input name="ctl00$txtName" type="text" id="txtName" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Email</label>
                    <input name="ctl00$TextBox1" type="text" id="TextBox1" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Mobile Number</label>
                    <input name="ctl00$TextBox2" type="text" id="TextBox2" class="form-control" /> </div>
                <div class="m-t-20 text-center">
                    <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include 'footer-scripts.php';?>
</body>

</html>