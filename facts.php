<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/slick.css" />
    <link rel="stylesheet" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/notify-bootstrap.css" />
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css" />
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/bar.chart.min.css" />
    <script type="text/javascript" src="assets/js/jquery.bar.chart.min.js"></script>
    <script type="text/javascript" src="assets/js/d3.v4.min.js"></script>
    <style>
        .chart_container .bar_item:nth-child(odd),
        .title-text {
            fill: #4c85b2 !important
        }

        .chart_container .bar_item:nth-child(even) {
            fill: #87bad7 !important
        }

        .legend_div ul li:nth-child(odd) div {
            background: #4c85b2 !important
        }

        .legend_div ul li:nth-child(even) div {
            background: #87bad7 !important
        }
    </style>
</head>

<body style="background: none">
<form method="post" action="#" id="form1">
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTY1NDU2MTA1MmRkRe9XhgPXueRRdwac7UW0pYzoZvq5UvlU2wJBEAd74AI=" /> </div>
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="52853DAB" />
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAATHOFBb/OaFxmeK0AJrI/NEt0GqqGmTunmU8BFOEDL5JLt0vKVmDtQYb7KLGs2qWD1/2i7OlKCH0kBh9oGA4EXWo7j7CYPluUpq4eWkaSqnFhLSQiIrk4fHwPnUVuov1gs=" /> </div>
    <div>
        <?php include 'header.php';?>
        <div id="myButton"></div>
    </div>
    <div class="page-banner"> <img src="assets/images/sub1.jpg" alt="sub-banner" width="100%" class="sub-banner" />
        <h1 class="h1-banner text-uppercase">
            Facts & Figures About K12 Segment</h1> </div>
    <div class="page">
        <div class="container">
            <div class="m-t-100 m-b-100">
                <ul class="list-none wow fadeInLeft">
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> K12 industry is second in the category after higher education in terms of market size.</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Govt push for private players to enter the market</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Many new entrants have entered into this segment in las 10 years</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Market size in 2013 was $4bn and is expected to touch $144 bn by 2022</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> There are around 260 mn population in the age group of 5 yr to 14 yr.</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Parents preference of sending their children into private schools (40% children get into 20% private schools)</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> We need 10000+ new K12 schools in India</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Schools have a much higher stickiness in terms of the students staying with them as it’s a 10 yr commitment from the parents</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Brand consciousness and digital revolution are fast catching up in this industry</p>
                    </li>
                </ul>
                <div class="m-t-50 wow fadeInRight">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-sm-12 col-12">
                            <div class="table-container">
                                <h3 class="h3-style">
                                    Population and Urbanisation Trend</h3>
                                <table class="table table-bordered m-t-50">
                                    <thead class="bgcolor">
                                    <tr>
                                        <th> Year </th>
                                        <th> Population (mn) </th>
                                        <th> Urban (mn) </th>
                                        <th> Urban (%) </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td> 2010 </td>
                                        <td> 1206 </td>
                                        <td> 373 </td>
                                        <td> 31% </td>
                                    </tr>
                                    <tr>
                                        <td> 2020 </td>
                                        <td> 1353 </td>
                                        <td> 471 </td>
                                        <td> 35% </td>
                                    </tr>
                                    <tr>
                                        <td> 2030 </td>
                                        <td> 1476 </td>
                                        <td> 583 </td>
                                        <td> 39% </td>
                                    </tr>
                                    <tr>
                                        <td> 2040 </td>
                                        <td> 1566 </td>
                                        <td> 701 </td>
                                        <td> 45% </td>
                                    </tr>
                                    <tr>
                                        <td> 2050 </td>
                                        <td> 1620 </td>
                                        <td> 814 </td>
                                        <td> 50% </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-sm-12 col-12">
                            <h3 class="h3-style">Market Size in BN$</h3>
                            <div id="chtAnimatedBarChart" class="bcBar m-t-50"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            var chart_data = getData();
            $('#chtAnimatedBarChart').animatedBarChart({
                data: chart_data
            });
        });
        getData = function() {
            return [{
                "group_name": "2013",
                "name": "Preschool",
                "value": 2
            }, {
                "group_name": "2013",
                "name": "School",
                "value": 44
            }, {
                "group_name": "2013",
                "name": "Higher Education",
                "value": 10.1
            }, {
                "group_name": "2020",
                "name": "Preschool",
                "value": 5.35
            }, {
                "group_name": "2020",
                "name": "School",
                "value": 144
            }, {
                "group_name": "2020",
                "name": "Higher Education",
                "value": 37.8
            }];
        }
    </script>
    <?php include 'footer.php';?>
    </div>
    </div>
    <div class="modal" id="modalPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    <h2 class="subtitle text-left">
                        For Admissions</h2>
                    <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
                </div>
                <div class="m-t-20">
                    <label class="form-label"> Name</label>
                    <input name="ctl00$txtName" type="text" id="txtName" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Email</label>
                    <input name="ctl00$TextBox1" type="text" id="TextBox1" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Mobile Number</label>
                    <input name="ctl00$TextBox2" type="text" id="TextBox2" class="form-control" /> </div>
                <div class="m-t-20 text-center">
                    <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include 'footer-scripts.php';?>
</body>

</html>