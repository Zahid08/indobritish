<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/slick.css" />
    <link rel="stylesheet" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/notify-bootstrap.css" />
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css" />
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
</head>

<body style="background: none">
<form method="post" action="#" id="form1">
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTY1NDU2MTA1MmRk6eeEH03VIZz3B+q7pfZQRTSd/HyGZ8Y04EY51upCuH4=" /> </div>
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DDE6907F" />
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAATCbT3wZI4xlPfHUhRHlSyHt0GqqGmTunmU8BFOEDL5JLt0vKVmDtQYb7KLGs2qWD1/2i7OlKCH0kBh9oGA4EXWeWH0p+P8O4EGwJNz6tvAG6sP+Poi6u3AYxLnaVY16Yo=" /> </div>
    <div>
        <?php include 'header.php';?>
        <div id="myButton"></div>
    </div>
    <div class="page-banner"> <img src="assets/images/sub1.jpg" alt="sub-banner" width="100%" class="sub-banner" />
        <h1 class="h1-banner">
            SCHOOL PROGRAMS</h1> </div>
    <div class="page">
        <div class="container">
            <div class="m-t-100 m-b-100">
                <h2 class="subtitle text-center">
                    Language Lab</h2>
                <div class="text-center"> <img src="assets/images/library.jpg" alt="library" /> </div>
                <p class="p-t-20"> Language lab helps students for pronunciation, spelling, Sentence structure, understanding the idioms / expressions from the world of languages. It is in essence of the convergence of text, images, audio, video, web and collaboration brought together in a seamlessly integrated learning platform. The language lab thus uses technology and international phonic programs to guide our young learners towards a future where they are never lost for words. </p>
                <p class="p-t-10"> The Language lab is equipped with - </p>
                <ul class="list-none">
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Computer based learning modules.</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Digital pens to make sure learning is easy and fun.</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Audio books and audio-based learning tools for beer pronunciation.</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> A vast library of audio and video media</p>
                    </li>
                </ul>
                <div class="p-t-50">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 wow fadeInUp">
                            <div class="gray-bg padd-20">
                                <h2 class="subtitle text-center">
                                    Innovation Lab</h2>
                                <p class="p-t-20 text-justify"> Innovation lab has all the latest global innovative learning tools for IBGS learners. It includes Virtual Reality instruments, Robotics learning tools,Artificial Intelligence & Internet of Things learning tools with qualified educators. Students love to visit this lab as it gives them infinite..opportunies to explore their inner talent and potential. </p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 wow fadeInDown">
                            <div class="gray-bg padd-20">
                                <h2 class="subtitle text-center">
                                    Art & Craft Lab</h2>
                                <p class="p-t-20 text-justify"> It is a place where students innovate on their artistic skills. Poetry, painting, sculpture is few of the many acvity's students enjoy here.With best-in-class educators to help students explore their artisc skills, IBGS learners gets the global platform and infinite opportunities in exploring their art & craft skills. </p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 wow fadeInUp">
                            <div class="gray-bg padd-20">
                                <h2 class="subtitle text-center">
                                    Computer lab</h2>
                                <p class="p-t-20 text-justify"> The computer lab is where our students not just learn computer but also get familiarised with technology. It is where we use technology to teach and our students explore the true potenal within. The labs have latest generation computers with upgraded version of learning tools right from basic office automation to design / animation. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
                <div class="p-t-50">
                    <h2 class="subtitle">
                        Mathematics Lab
                    </h2>
                    <div class="row p-t-20 wow rollIn">
                        <div class="col-xl-8 col-md-8 col-sm-12 col-12">
                            <p> This is where our students are able to learn the mathematics concept visually and practically, while learning to apply them in real life practices. Our students explore and play with numbers, equations and formulas, giving them the much-needed experience and perspective to view mathematics not just as a subject but as a tool for success. Few of the primary benefits of mathematics lab are as below</p>
                            <ul class="p-t-20 list-none">
                                <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                                    <p> Practical application of math theory and concepts.</p>
                                </li>
                                <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                                    <p> Part of the weekly timetable.</p>
                                </li>
                                <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                                    <p> Use of state-of-the-art software and instruments.</p>
                                </li>
                                <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                                    <p> Partnership with leading mathematics research groups.</p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xl-4 col-md-4 col-sm-12 col-12"> <img src="assets/images/math.jpg" alt="math-lab" /> </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
                <div class="p-t-50 wow fadeInLeft">
                    <h2 class="subtitle text-center">
                        Science Labs</h2>
                    <p class="p-t-20"> The best way to learn science is through actions and this is most vibrant place at Indo-Brish Global School. Right from visualising the experiments to conducng them, from seeing to understanding forms and structures, our IBGS learners are inculcated with the scienfic attitude that is utmost importance in today's world. </p>
                    <p class="p-t-20"> Indo-Brish Global Schools are equipped with 'Cyber Science' soware that allows interactive exploration of the science. Cyber Science 3D is a comprehensive package of detailed, interacve virtual realiexperiences focused on aiding in the learning and exploration of natural phenomena, biological concepts, physics, and mechanical systems as well as many more. Cyber Science 3D provides a unique, immersive, and visual experience </p>
                    <p class="p-t-20"> that allows students to observe and interact with high fidelity models of structures in full 3D for all kinds of learning applicaons. Causeand effect simulaons allow the IBGS learner to learn at their own pace, which is the essence of our Science Lab. </p>
                    <p class="p-t-20"> Addionally, our laboratories are equipped with the necessary equipment, regents, specimens and highly experienced and trained staff that ensure the safety of students while allowing them to explore the world of Science. </p>
                </div>
                <div class="p-t-20 wow fadeInRight">
                    <div class="row">
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                            <h2 class="subtitle">
                                Library Labs</h2>
                            <p> At Indo-Brish Global School, the library is not a sanctum of silence but a world of adventure. With state of art two storied library, Students gets all kind of corners like comic book secon with fun based sing arrangement, Tiny tots' corner, silence zone, etc. With an expansive collecon of books on every facet of every subject, Indo-Brish Global School take a no-expense-spared approach to stocking the library. Since our philosophy is with 3H & 4P where our students ask quesons, it is enquiry based educaon, we also inculcate the curiosity of research and give them the perfect space for finding out more about everything.</p>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12"> <img src="assets/images/lib.jpg" alt="library" width="100% " /> </div>
                    </div>
                </div>
                <div class="p-t-20 wow rollIn">
                    <div class="row">
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                            <h2 class="subtitle">
                                Student Exchange Programme</h2>
                            <p> The student exchange programme within the IBGS and Vaisvik Shodh group of school's is a unique inititiave in fostering bonding and collaboration. By leveraging the scale of our school network, IBGS and group school students are given the opportunity to attend classes at any of our locations. This gives them an immersive environment to learn from other cultures and traditions.</p>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12"> <img src="assets/images/student.jpg" alt="student" width="100%" /> </div>
                    </div>
                </div>
                <div class="p-t-20 wow fadeInDown">
                    <h2 class="subtitle">
                        The Sports Club</h2>
                    <p class="p-t-20"> The sports club is aimed at developing fundamental skills such as focus, strategic thinking and leadership through the pracce of sporng discipline. Our ensemble of sporng acvies include:</p>
                    <ul class="list-none p-t-20">
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> New age sports such as skang, archery, fencing and golf </p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Tradional sports such as basketball, cricket, hockey and football</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Coaches are selected aer careful scruny to ensure that experience, knowledge, teaching skills are brought together for best-in-class experience to students.</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Each extracurricular acvity has a carefully constructed syllabus which is taught in most effecve way to ensure the students enjoy it and develop the sportsmanship spirit</p>
                        </li>
                    </ul>
                </div>
                <div class="p-t-20 wow rollIn">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h2 class="subtitle">
                                Indo-British Olympiad</h2>
                            <p class="p-t-20"> At IBGS, we believe that excellence and leaderships can be learned. To facilitate this, we leverage our PAN India presence along with our group schools to conduct inter school competitions and cultural events where every IBGS student compete with others nationally with wits, spirits and endurance </p>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h2 class="subtitle">
                                Holistic Growth
                            </h2>
                            <p class="p-t-20"> We understand that all work and no play make our young learners sfled and our extracurricular activities are aimed at addressing this very factor of school life.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'footer.php';?>
    </div>
    </div>
    <div class="modal" id="modalPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    <h2 class="subtitle text-left">
                        For Admissions</h2>
                    <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
                </div>
                <div class="m-t-20">
                    <label class="form-label"> Name</label>
                    <input name="ctl00$txtName" type="text" id="txtName" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Email</label>
                    <input name="ctl00$TextBox1" type="text" id="TextBox1" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Mobile Number</label>
                    <input name="ctl00$TextBox2" type="text" id="TextBox2" class="form-control" /> </div>
                <div class="m-t-20 text-center">
                    <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include 'footer-scripts.php';?>
</body>

</html>