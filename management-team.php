<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/slick.css" />
    <link rel="stylesheet" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/notify-bootstrap.css" />
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css" />
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
</head>

<body style="background: none">
<form method="post" action="" id="form1">
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTY1NDU2MTA1MmRkeiPMpHL1NV0Dxho1IoQ0+XB71LUnpXsO/4R0Udo4tDE=" /> </div>
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0F27E254" />
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAT1CNvxO3xRErFzaX+J2h3Yt0GqqGmTunmU8BFOEDL5JLt0vKVmDtQYb7KLGs2qWD1/2i7OlKCH0kBh9oGA4EXWHyqp+lh3SBR4X8j76+DRGiz5zVz0vunb6i80fkafsO4=" /> </div>
    <div>
        <?php include 'header.php';?>
        <div>
            <div id="myButton"></div>
        </div>
        <div class="page-banner"> <img src="assets/images/sub1.jpg" alt="sub-banner" width="100%" class="sub-banner" />
            <h1 class="h1-banner">
                OUR TEAM MEMBERS</h1> </div>
        <div class="m-t-50 m-b-100 page-content">
            <div class="container">
                <div class="p-t-50">
                    <div class="box-line row">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12"> <img src="assets/images/p4.png" alt="staff-images" width="100%" />
                            <h3 class="m-t-20 h3-style text-center">
                                Dr. Leena Pimpley</h3>
                            <p class="m-t-10 text-center"> Academic Mentor</p>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                            <p class="text-justify"> Dr. Leena holds 30 years of experience in the field of education. She has set up K-12 schools aliated with IB, Cambridge, CBSE and ICSE boards across India. She has also led renowned international schools in Mumbai as Principal and Programme Head. She has been associated with national and international networks of schools such as Aga Khan Education Service as Global Leader, Zee Learn Ltd as Head of Academics. She has designed curricula aimed at holistic development of every child. She is a NABET empaneled school quality auditor and British Council empaneled consultant. </p>
                            <p><span class="sp-txt">Vision </span>- Be a part of and leave behind legacy of a peacefully co-existing and evolving eco-system exhibiting human endeavor towards sustainable development rooted in the belief ‘वसुधैव कटबकम’् (‘The Earth is One Family’. All living beings, not just humans, and all non-living resources, matter, and energy have every right to exist and thrive on Mother Earth) </p>
                            <p><span class="sp-txt"> Mission </span>- Contribute actively towards the creation of a system for educating young adults to focus on and transcend professional success with a goal to imbibe essential life skills and attitudes such as Socio-Emotional Awareness, Mindfulness, Responsibility and Resilience. The system to stand firmly on the Four Fundamental Pillars of Learning: To Know, To Do, To Be and To Live Together. </p>
                            </p>
                        </div>
                    </div>
                    <div class="box-line row">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12"> <img src="assets/images/p6.png" alt="staff-images" width="100%" />
                            <h3 class="m-t-20 h3-style text-center">
                                Dr. Namrata Majhal</h3>
                            <p class="m-t-10 text-center"> Deputy General Manager - Academics</p>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                            <p class="text-justify"> Dr Namrata Majhail ,a TEDx speaker, bears an exceptionally posive and pleasing persona. She believes in Learning along with 21st in learning along with our 21 century learners . She has been conferred with a Doctorate in the field of Education Management for her whole hearted engagement in this field .She has Master's Degree both in English and Education. She has completed a programme in “Strategic Leadership for Schools in a Changing Environment” from IIM Ahmedabad .Besides being a certified Six Sigma White belt in Education field she is also a certified Master Trainer at National and International level. She has the experience of being a Consultant Grader and Content Developer with a leading Publication House. </p>
                            <p class="text-justify p-t-10"> Dr Namrata is an academician with around 11 years of leadership experience out of 30 years of teaching students from KG to PG level from different educaon Boards and from diverse background in and outside country </p>
                            <p class="text-justify p-t-10"> Dr. Majhail has earned herself many accolades and awards. She is an experienced CBSE Master trainer with knowledge of ground level realies as a hands-on leader and has a solution-oriented approach in class room management and mastery over effective teaching learning methodologies. As a former President of Sahodya Dr Majhail has worked very closely with COE Pune to conduct many crucial workshops at regional level. </p>
                            <p class="text-justify p-t-10"> She has earned several awards – Education Legend Award, Excellence Awards along with a Life Time Achievement Award in the field of Education to name a few! </p>
                            <p class="text-justify p-t-10"> She is a member of Indian Didactics Association( IDA), Education Leaders Community and Centre for Education Development Delhi ( CED). </p>
                            <p class="text-justify p-t-10"> She has conducted numerous online and offline workshops on the subjects related to academic realies and requirements. She has conducted Seminars, Webinars and Talk shows with students and experts. She has been invited as panelists and a speaker on many forums to share her views on NEP and on the during and post lockdown scenarios in schools. </p>
                        </div>
                    </div>
                    <div class="box-line row">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12"> <img src="assets/images/p5.png" alt="staff-images" width="100%" />
                            <h3 class="m-t-20 h3-style text-center">
                                Dr. Ritu Uppal
                            </h3>
                            <p class="m-t-10 text-center"> Academic Excellence</p>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                            <p class="text-justify"> Over 30 years of academic experience form K-12 to post graduation level. PhD in Educational Technology. Qualified Instruconal designer with over 15 years experience in Curriculum design, development ,implementation and teacher training. Expert in of face to face, Blended and online content development and delivery. Working as consultant, mentor and vising faculty in various national and international organization. Director - Academics MAF Technologies(Mafatlal Group) Nominated and working as a Mentor ,second year in a row, for Open Education for Beer World (OE4BW) project of UNESCO Chair Slovenia in collaboration with University of Nova Garcia, Slovenia. Member, Ad-hoc Board of Studies in H.S.N.C University in the subject Educational Technology and Research Methodology ( BTTC College ) Programme Leader of Cambridge International Diploma in Teaching in Learning. Invited to conduct seminars and workshops fo Higher education Teacher training I the field of Contemporary teaching learning pedagogy, Information Communication And Technology(ICT), Online course development. MOOCs. </p>
                        </div>
                    </div>
                    <div class="box-line row">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12"> <img src="assets/images/p8.png" alt="staff-images" width="100%" />
                            <h3 class="m-t-20 h3-style text-center">
                                Ms. Yayati Shah
                            </h3>
                            <p class="m-t-10 text-center"> Academic Head - Pre School Segment</p>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                            <p class="text-justify"> Yayati, is an exceptional preprimary educator with qualification BA, D. EL. ED, Montessori Trained, Specialization in Learning Disability. An experienced, passionate and committed educator. She is extraordinarily talented at connecting with her students and educating in a creative and engaging fashion. After her stint as a passionate Kinder Garten teacher for 12 years, she took the role of IEYC Coordinator. Her focus was to inspire, excite and nurture children holistically. She believes that a learning programme that embeds creative and fun activities ensures holistic development through freedom of expression communication and collaboration. She has also trained and groomed the prospective Preschool teachers with her expertise in the field of Pre- Primary Education </p>
                        </div>
                    </div>
                    <div class="box-line row">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12"> <img src="assets/images/p9.png" alt="staff-images" width="100%" />
                            <h3 class="m-t-20 h3-style text-center">
                                Mr. Kiran Mugal
                            </h3>
                            <p class="m-t-10 text-center"> Operation Head </p>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12">
                            <p class="text-justify"> Kiran, a civil engineering graduate with over 11+ years of experience in Infrastructure Project Execution at reputed organizations across India & other Asian countries. Mr. Kiran Mugal is the incharge of creative controlling of the civil and interiors of school Projects, Ensuring quality control and time line with the team of effective professionals. He has Pursued the designing and implementation of many educational infrastructure projects domestically and internationally. He has completed 17+ Schools projects with over 700000 sq ft. of built up space. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'footer.php';?>
    </div>
    </div>
    <div class="modal" id="modalPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    <h2 class="subtitle text-left">
                        For Admissions</h2>
                    <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
                </div>
                <div class="m-t-20">
                    <label class="form-label"> Name</label>
                    <input name="ctl00$txtName" type="text" id="txtName" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Email</label>
                    <input name="ctl00$TextBox1" type="text" id="TextBox1" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Mobile Number</label>
                    <input name="ctl00$TextBox2" type="text" id="TextBox2" class="form-control" /> </div>
                <div class="m-t-20 text-center">
                    <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include 'footer-scripts.php';?>
</body>

</html>