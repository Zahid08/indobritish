<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/slick.css" />
    <link rel="stylesheet" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/notify-bootstrap.css" />
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css" />
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script type="text/javascript">
        var onloadCallback = function() {
            alert("grecaptcha is ready!");
        };

        function validationNumeric(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if(charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else {
                return true;
            }
        }
    </script>
</head>

<body style="background: none">
<div class="aspNetHidden">
    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE3MTIyNjgyMDMPZBYCZg9kFgICAw9kFgICAQ9kFgICCQ8PFgIeBFRleHRlZGRkaVDHQ4daDYjsEMkvX2dmissI7Zoys+wgfX6MSnQa+Gw=" /> </div>
<div class="aspNetHidden">
    <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CD2448B2" />
    <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAnH8XPcmSnvxEgNd1HXClqKVes4AfNrEwZQ25wFmngHnxp33ayz7pzV7KfF82ugVEeqQLpX+aV+Mguiu0v7wnea0MESdjp/mirK+KbPlTyISnKoEn7nSAySghxZ7acXGhC3QaqoaZO6eZTwEU4QMvkku3S8pWYO1BhvsosazapYPX/aLs6UoIfSQGH2gYDgRdbblqByoMnu3TgovgHRbXatIcNywt9negThfPz5KsgsSA==" /> </div>
<div>
    <?php include 'header.php';?>
    <div id="myButton"></div>
</div>
<div class="page-banner"> <img src="assets/images/sub1.jpg" alt="sub-banner" width="100%" class="sub-banner" />
    <h1 class="h1-banner">
        CONTACT US</h1> </div>
<div class="page">
    <div class="container">
        <div class="m-t-100 m-b-100">
            <form method="POST">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <h2 class="subtitle">
                            INDO BRITISH GLOBAL SCHOOL, PUNE</h2>
                        <div class="p-t-20">
                            <p> 170, Dnyaneshwar Colony,
                                <br /> Opp. N.D Tower's,
                                <br /> Behind PCMC Hospital, Akurdi, Pune, 411035</p>
                        </div>
                        <p> <a href="#" class="align-baseline" style="color: #666"><i class="fas fa-envelope"></i>
                                <span class="m-l-10">For Admission :<br />enquiry@indo-british.com</span></a></p>
                        <p> <a href="#" class="align-baseline" style="color: #666"><i class="fas fa-envelope"></i>
                                <span class="m-l-10">For Franchisee : partner@vaishvikshodh.com </span></a> </p>
                        <p> <a href="#" class="align-baseline" style="color: #666"><i class="fas fa-phone-alt"></i>
                                <span class="m-l-10">+91 +91 775599 0720/21 </span></a> </p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12" id="contact_form_submitt">
                        <h2 class="subtitle">
                            FOR MORE ENQUIRY</h2>
                        <div class="p-t-20">
                            <label class="form-label"> Name <span class="star">* </span></label>
                            <input name="contact_name" type="text" id="contact_name" class="form-control" placeholder="Enter Your Name" onfocus="this.placeholder=&#39;&#39;" /> </div>
                        <div class="p-t-20">
                            <label class="form-label"> Email <span class="star">* </span></label>
                            <input name="contact_email" type="text" id="contact_email" class="form-control" onfocus="this.placeholder=&#39;&#39;" placeholder="Enter Your Email" />
                            <p id="email_eror" style="color: red"></p>
                        </div>
                        <div class="p-t-20">
                            <label class="form-label"> Mobile Number <span class="star">* </span></label>
                            <input name="contact_mobile" type="number" id="contact_mobile" class="form-control" placeholder="Enter Your Mobile Number" onfocus="this.placeholder=&#39;&#39;" onkeypress="return validationNumeric(this);" min="10" max="12"/> </div>
                        <div class="p-t-20">
                            <label class="form-label"> Message <span class="star">* </span></label>
                            <input name="contact_message" type="text" id="contact_message" class="form-control" placeholder="Enter Your Message" onfocus="this.placeholder=&#39;&#39;" /> </div>
                        <div class="m-t-20" style="padding-left: 15px">
                            <div class="g-recaptcha" data-sitekey="<?=$capchaSiteKey?>"> </div>
                        </div>
                        <div id="thankyoumessage" style="color: green"></div>
                        <div class="p-t-20">
                            <input type="button" name="submit_contact" value="Send message" id="contact_form_btn" class="btn btn-primary y-clr" /> </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="maps p-t-50"> </div>
    </div>
</div>

<?php include 'footer.php';?>
</div>
</div>
<div class="modal" id="modalPopup">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-head">
                <h2 class="subtitle text-left">
                    For Admissions</h2>
                <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
            </div>
            <div class="m-t-20">
                <label class="form-label"> Name</label>
                <input name="ctl00$txtName" type="text" id="txtName" class="form-control" /> </div>
            <div class="m-t-20">
                <label class="form-label"> Email</label>
                <input name="ctl00$TextBox1" type="text" id="TextBox1" class="form-control" /> </div>
            <div class="m-t-20">
                <label class="form-label"> Mobile Number</label>
                <input name="ctl00$TextBox2" type="text" id="TextBox2" class="form-control" /> </div>
            <div class="m-t-20 text-center">
                <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
            </div>
        </div>
    </div>
</div>

<?php include 'footer-scripts.php';?>

<script>
    $(function() {

        $("input#contact_message").keyup(function(){
            if ($(this).val()==''){
                $('input#contact_message').css("border", "1px solid red");
            }else {
                $('input#contact_message').css("border", "1px solid green");
            }
        });

        $("input#contact_name").keyup(function(){
            if ($(this).val()==''){
                $('input#contact_name').css("border", "1px solid red");
            }else {
                $('input#contact_name').css("border", "1px solid green");
            }
        });

        $("input#contact_email").keyup(function(){
            if ($(this).val()==''){
                $('input#contact_email').css("border", "1px solid red");
            }else {
                $('input#contact_email').css("border", "1px solid green");
            }

            if (isValidEmailAddress($(this).val()) && $(this).val()!=''){
                $('input#contact_email').css("border", "1px solid green");
                $('p#email_eror').html("");
            }

        });

        $("input#contact_mobile").keyup(function(){
            if ($(this).val()==''){
                $('input#contact_mobile').css("border", "1px solid red");
            }else {
                $('input#contact_mobile').css("border", "1px solid green");
            }
        });


        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test(emailAddress);
        }

        $('#contact_form_btn').on('click', function(e){

            var baseUrl                             ='<?=$baseUrl?>';
            var name                                =$('input#contact_name').val();
            var email                               =$('input#contact_email').val();
            var mobileno                            =$('input#contact_mobile').val();
            var contact_message                     =$('input#contact_message').val()||'';

            var error=0;

            if (contact_message==''){
                $('input#contact_message').css("border", "1px solid red");
                error=1;
            }else {
                $('input#contact_message').css("border", "1px solid green");
            }

            if (name==''){
                $('input#contact_name').css("border", "1px solid red");
                error=1;
            }else {
                $('input#contact_name').css("border", "1px solid green");
            }

            if (email==''){
                $('input#contact_email').css("border", "1px solid red");
                error=1;
            }else {
                $('input#contact_email').css("border", "1px solid green");
            }

            if (mobileno==''){
                $('input#contact_mobile').css("border", "1px solid red");
                error=1;
            }else {
                $('input#contact_mobile').css("border", "1px solid green");
            }

            if (!isValidEmailAddress(email) && email!=''){
                $('input#contact_email').css("border", "1px solid red");
                $('p#email_eror').html("Invalid email address");
            }

            if (error==1){
                return false;
            }

            if (error==0){
                $.ajax({
                    url:baseUrl+'/email/contact_email.php',
                    type: "POST",
                    data: {
                        name  :name,
                        email: email,
                        mobileno: mobileno,
                        contact_message: contact_message,
                    },
                    success: function(data){
                        if (data==1){
                            $('div#contact_form_submitt').html("<h2  class='Sucess-message'>Thank you for writing to us. Our representative will soon contact you and address your queries shortly</h2>");
                        }
                    },
                    error: function(){
                        alert("Something went wrong")
                    }
                });
            }
        });
    });
</script>
</body>

</html>