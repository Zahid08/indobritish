<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="ms-icon-144x144.php" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/slick.css" />
    <link rel="stylesheet" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/notify-bootstrap.css" />
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css" />
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
</head>

<body style="background: none">
<form method="post" action="https://indo-british.com/about.php" id="form1">
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTY1NDU2MTA1MmRkl/wKe+PBzSiEXiXvlAt5ZvriDdmT6XepvpDjBF7FUNY=" /> </div>
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E809BCA5" />
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAASbWP416/zq2A3g2RoqxL8Bt0GqqGmTunmU8BFOEDL5JLt0vKVmDtQYb7KLGs2qWD1/2i7OlKCH0kBh9oGA4EXWpMGLJE711xLZBmya4J+hDbM8MHMk/XPVBICQpbLTjW8=" /> </div>
    <div>
        <?php include 'header.php';?>
        <div>
            <div id="myButton"></div>
        </div>
        <div class="page-banner"> <img src="assets/images/sub1.jpg" alt="sub-banner" width="100%" class="sub-banner" />
            <h1 class="h1-banner">
                ABOUT US</h1> </div>
        <div class="page">
            <div class="container">
                <div class="m-t-100">
                    <p> Indo-British Global School is an Initiative of Vaisvik Shodh Pvt. Ltd. Led by Mr. Shivgir Gir and Dr. Dhananjay Varnekar to prepare Global citizens through its unique style of teaching learning pedagogy “ECOLIER INFINITY”. Vaisvik Shodh promoters have been contributing for many years to words nurturing students form diverse background. The education veteran Mr. Shivgir has established 100+ reputed K12 schools in the country and has contributed in nation building, whereas Dr. Dhananjay has been running education institutes from Kg to PG and Youth programs over last 34 years. We are amongst the first few to introduce integrated curricular schools in India with a unique vision of educating children with global perspectives to learn.To fulfil our vision of developing children with self-excellence, happy and future ready global citizens, it was imperative that we bring innovations in schooling. Indo-British Global School (IBGS) is one such stapes in that direction.</p>
                    <div class="p-t-50">
                        <div class="row">
                            <div class="col-xl-8 col-lg-8 col-md-6 col-sm-12 col-12">
                                <h2 class="subtitle">
                                    OUR <span>MISSION</span></h2>
                                <p class="p-t-20"> To provide qualitative, real-me and experiential education to develop lifelong, confident learners who would rely on self-excellence and would become happy and futuristic global citizens.</p>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 text-right">
                                <object data="assets/SVG/mission.svg" type="image/svg+xml" style="width: 200px"> </object>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-t-50">
                <div class="gray-bg p-t-50 p-b-50 wow rollIn">
                    <div class="container">
                        <h2 class="subtitle text-center">
                            OUR CORE <span>COMMITMENTS</span></h2>
                        <ul class="list-none p-t-30">
                            <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                                <p> Modelling & Scaffolding by adults</p>
                            </li>
                            <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                                <p> Offering space and scope for experimenting, failing, retrying, risk taking, celebrating efforts and achievement</p>
                            </li>
                            <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                                <p> Motivating to communicate, collaborate and lead</p>
                            </li>
                            <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                                <p> Nurturing personal values: Integrity, Resilence, Excellence, Mindfulness, caring</p>
                            </li>
                            <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                                <p> Transferring gamut of tools for thinking, feeling, & taking action for sustainable existence</p>
                            </li>
                        </ul>
                        <div class="p-t-50">
                            <h2 class="subtitle">
                                OUR BELIEF <span>SYSTEM</span>
                            </h2>
                            <p class="p-t-20"> Endeavor of Self Excellence: There is nothing noble in being superior to your fellow man: True nobility is being superior to your former self. Ecolier Infinity Learning Approach believes in self-excellence as against compeng with others. We are commied to equipping every child in our care to imbibe the essenal competencies, proficiencies, mindset and values coupled with the ability to look within and strive to beer oneself. Self Excellence is at the core of IBGS. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'footer.php';?>
    </div>
    </div>
    <div class="modal" id="modalPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    <h2 class="subtitle text-left">
                        For Admissions</h2>
                    <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
                </div>
                <div class="m-t-20">
                    <label class="form-label"> Name</label>
                    <input name="ctl00$txtName" type="text" id="txtName" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Email</label>
                    <input name="ctl00$TextBox1" type="text" id="TextBox1" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Mobile Number</label>
                    <input name="ctl00$TextBox2" type="text" id="TextBox2" class="form-control" /> </div>
                <div class="m-t-20 text-center">
                    <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include 'footer-scripts.php';?>
</body>
</html>