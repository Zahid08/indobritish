<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/slick.css" />
    <link rel="stylesheet" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/notify-bootstrap.css" />
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css" />
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
</head>

<body style="background: none">
<form method="post" action="" id="form1" enctype="multipart/form-data">
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE3ODI2MzI5NjgPZBYCZg9kFgICAw8WAh4HZW5jdHlwZQUTbXVsdGlwYXJ0L2Zvcm0tZGF0YWRk1KTQt8OOhtVAX0x67AekBKzo+tdzw/JVt1tbr5cs3yU=" /> </div>
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2415B15E" />
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAj0koW85ao6WUbk8m3RFaJFWsgvNTU9pqb9yeWEwIaCs0tQ2Xn9pkQQJ6kS5PkdsWvj2z6Bri0w5u11Oaj0Myjzq1c4+VWaxQieYN0UgPTjsrdBqqhpk7p5lPARThAy+SS7dLylZg7UGG+yixrNqlg9f9ouzpSgh9JAYfaBgOBF1pIFaEumOXM4e75aP1jvrVN4UkJEW6190/sZcAXAk4tG" /> </div>
    <div>
        <?php include 'header.php';?>
        <div id="myButton"></div>
    </div>
    <div class="page-banner"> <img src="assets/images/sub1.jpg" alt="sub-banner" width="100%" class="sub-banner" />
        <h1 class="h1-banner">
            CAREERS</h1> </div>
    <div class="page">
        <div class="container">
            <div class="m-t-100">
                <p> Indo-Brish Global School is an endeavour by Vaisvik Shodh Pvt. Ltd. Led by Mr. Shivgir Gir and Dr. Dhananjay Varnekar to prepare Global citizens through its unique style of teaching learning pedagogy “ECOLIER INFINITY”.</p>
                <p class="p-t-20"> Vaisvik Shodh promoters have been contributed many years for nurturing students of different age group across the country. The education veteran Mr. Shivgir has established 100+ good quality K12 schools in the country and contributed in nation building, whereas Dr. Dhananjay has been running education institutes from Kg to PG and Youth programs over last 34 years. We have introduced amongst the first few integrated curricular schools in India with a unique vision of educating children with global perspectuives to learn. </p>
                <p class="p-t-20"> To fulfil our vision of developing the children with self-excellence, happy and futuristic global citizens, it was imperative that we bring innovations in schooling. Indo-Brish Global School (IBGS) has been our endeavour in that direction. </p>
            </div>
        </div>
        <div class="p-t-50 wow fadeInU">
            <div class="gray-bg">
                <div class="container p-b-50">
                    <div style="width:50%;margin:0 auto">
                        <h2 class="subtitle p-t-50 text-center">DROP YOUR RESUME. OUR RECRUITER WILL GET BACK SOON</h2>
                        <div class="p-t-50">
                            <div class="p-t-10">
                                <label class="form-label">Name</label>
                                <input name="ctl00$ContentPlaceHolder1$textBox" type="text" value="Enter Your Name" id="ContentPlaceHolder1_textBox" class="form-control" /> </div>
                            <div class="p-t-10">
                                <label class="form-label">Email</label>
                                <input name="ctl00$ContentPlaceHolder1$textBox1" type="text" value="Enter Your Email" id="ContentPlaceHolder1_textBox1" class="form-control" /> </div>
                            <div class="p-t-10">
                                <label class="form-label">Mobile Number</label>
                                <input name="ctl00$ContentPlaceHolder1$textBox2" type="text" value="Enter Your Mobile Number" id="ContentPlaceHolder1_textBox2" class="form-control" /> </div>
                            <div class="p-t-10">
                                <input type="file" name="ctl00$ContentPlaceHolder1$fuResume" id="ContentPlaceHolder1_fuResume" /> </div>
                            <div class="p-t-10 text-center">
                                <input type="submit" name="ctl00$ContentPlaceHolder1$btnsubmit" value="Submit" id="ContentPlaceHolder1_btnsubmit" class="btn btn-submit" /> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'footer.php';?>
    </div>
    </div>
    <div class="modal" id="modalPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    <h2 class="subtitle text-left">
                        For Admissions</h2>
                    <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
                </div>
                <div class="m-t-20">
                    <label class="form-label"> Name</label>
                    <input name="ctl00$txtName" type="text" id="txtName" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Email</label>
                    <input name="ctl00$TextBox1" type="text" id="TextBox1" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Mobile Number</label>
                    <input name="ctl00$TextBox2" type="text" id="TextBox2" class="form-control" /> </div>
                <div class="m-t-20 text-center">
                    <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include 'footer-scripts.php';?>
</body>

</html>