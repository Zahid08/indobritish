<?php include 'config.php';?>
<div class="wrapper">
    <div class="header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <div class="logo"> <img src="assets/images/logo.jpg" alt="indo-british-logo" /> </div>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12 f-column">
                    <div class="logo-information">
                        <h2 class="logo-title text-uppercase">
                            INDO-BRITISH GLOBAL SCHOOL</h2>
                        <p class="f-right"> <span><i class="fas fa-phone-alt"></i></span><span style="margin-left: 10px">+91 775599 0720 / 21 </span><span style="margin-left: 20px"><span>&nbsp;&nbsp;| &nbsp; &nbsp;</span> <i class="fas fa-envelope"></i></span><span style="margin-left: 10px">enquiry@indo-british.com</span> </p>
                    </div>
                    <div class="navigation">
                        <div class="row">
                            <div class="width100">
                                <nav class="navbar navbar-expand-lg navbar-light">
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul class="navbar-nav">
                                            <li class="nav-item active"><a class="nav-link" href="home.php">Home <span class="sr-only">
                                                        (current)</span></a> </li>
                                            <li class="nav-item dropdown  menu-large"><a class="nav-link dropdown-toggle" href="#" id="A2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    About Us</a>
                                                <div class="dropdown-content" aria-labelledby="navbarDropdown">
                                                    <div class="row">
                                                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12"> <a class="dropdown-item" href="about.php">About Us</a> <a class="dropdown-item" href="chairman.php">Chairman
                                                                Message</a> <a class="dropdown-item" href="management-team.php">Management Team</a> </div>
                                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                                            <object data="assets/svg/infra_1.svg" type="image/svg+xml" style="height: 64px; width: 64px;"> </object>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown menu-large"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Academics</a>
                                                <div class="dropdown-content" aria-labelledby="navbarDropdown">
                                                    <div class="row">
                                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12"> <a class="dropdown-item" href="pre-primary.php">Pre Primary Program</a> <a class="dropdown-item" href="primary.php">Primary Program</a> <a class="dropdown-item" href="middle.php">
                                                                Middle Program</a> <a class="dropdown-item" href="secondary.php">Secondary Program</a> <a class="dropdown-item" href="srsecondary.php">SrSecondary Program</a> </div>
                                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12"> <a class="dropdown-item" href="emerging-student.php">ECOLIER INFINITY</a> <a class="dropdown-item" href="learning-approach.php">Learning Approach</a> <a class="dropdown-item" href="programs.php">
                                                                School Programs</a>
                                                            <object data="assets/svg/books.svg" type="image/svg+xml" style="height: 64px; width: 64px;"> </object>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown menu-large"><a class="nav-link dropdown-toggle" href="#" id="A1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Admissions</a>
                                                <div class="dropdown-content" aria-labelledby="navbarDropdown">
                                                    <div class="row">
                                                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12"> <a class="dropdown-item" href="admission.php">Admissions</a> <a class="dropdown-item" href="guidelines.php">Guidelines and Procedures</a> <a class="dropdown-item" href="entry-criteria.php">
                                                                Entry Criteria</a> <a class="dropdown-item" href="admission-process.php">Admission
                                                                Process</a> <a class="dropdown-item" href="policies.php">Withdrawal Policy</a> </div>
                                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                                            <object data="assets/svg/policy.svg" type="image/svg+xml" style="height: 64px; width: 64px;"> </object>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown menu-large"><a class="nav-link dropdown-toggle" href="#" id="A3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Features & Facilities</a>
                                                <div class="dropdown-content" aria-labelledby="navbarDropdown">
                                                    <div class="row">
                                                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12"> <a class="dropdown-item" href="sports.php">Sports</a> <a class="dropdown-item" href="campus.php">
                                                                Campus</a> <a class="dropdown-item" href="classes.php">High Tech Classes</a> <a class="dropdown-item" href="transport.php">Transport Facilities</a> </div>
                                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                                            <object data="assets/svg/features.svg" type="image/svg+xml" style="height: 64px;
                                                                        width: 64px;"> </object>
                                                        </div>
                                                    </div>
                                            </li>
                                            <li class="nav-item dropdown menu-large"><a class="nav-link dropdown-toggle" href="#" id="A4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Parent's Corner</a>
                                                <div class="dropdown-content" aria-labelledby="navbarDropdown">
                                                    <div class="row">
                                                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12"> <a class="dropdown-item" href="calendar.php">School Calendar</a> <a class="dropdown-item" href="gallery.php">Gallery</a> <a class="dropdown-item" href="newsevents.php">News
                                                                & Events</a> <a class="dropdown-item" href="notifications.php">School Notifications</a> </div>
                                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                                            <object data="assets/svg/trophy.svg" type="image/svg+xml" style="height: 64px; width: 64px;"> </object>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown menu-large"><a class="nav-link dropdown-toggle" href="#" id="A5" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Contact Us</a>
                                                <div class="dropdown-content" aria-labelledby="navbarDropdown">
                                                    <div class="row">
                                                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12"> <a class="dropdown-item" href="contact.php">Contact Us</a> <a class="dropdown-item" href="career.php">Careers</a> </div>
                                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                                            <object data="assets/svg/contact.svg" type="image/svg+xml" style="height: 64px; width: 64px;"> </object>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown menu-large"><a class="nav-link dropdown-toggle" href="#" id="A6" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Partner With Us</a>
                                                <div class="dropdown-content" aria-labelledby="navbarDropdown">
                                                    <div class="row">
                                                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12"> <a class="dropdown-item" href="facts.php">Facts & Figures about K12 segment</a> <a class="dropdown-item" href="why-ibgs.php">Why Indo-British Global School?</a> <a class="dropdown-item" href="requirements.php">Requirements to start a K12 School</a> </div>
                                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                                            <object data="assets/svg/trophy.svg" type="image/svg+xml" style="height: 64px; width: 64px;"> </object>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="social-ic">
        <div>
            <ul class="so-icons">
                <li><a href="https://www.facebook.com/IndoBritishGlobal" class="f-bg clr-fff"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="https://twitter.com/IndoBritishGS" class="t-bg clr-fff"><i class="fab fa-twitter"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UChWaOGQYRcA7dYd2kOqjM2w" class="y-bg clr-fff"><i class="fab fa-youtube"></i></a></li>
                <li><a href="https://www.instagram.com/indo.british.global.school/" class="i-bg clr-fff"><i class="fab fa-instagram"></i></a></li>
                <li><a href="https://www.linkedin.com/company/indo-british-global-school" class="f-bg clr-fff"><i class="fab fa-linkedin-in"></i></a></li>
            </ul>
        </div>
    </div>
</div>