<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="ms-icon-144x144.php" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/slick.css" />
    <link rel="stylesheet" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/notify-bootstrap.css" />
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css" />
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
</head>

<body style="background: none">
<form method="post" action="https://indo-british.com/pre-primary.aspx" id="form1">
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTY1NDU2MTA1MmRkZziLHRkCib2iofxAud7x54b2MdoQAzMGgw6orlvhtYY=" /> </div>
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4E035346" />
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAASXwx7TxVUBdKBh9P58IlUQt0GqqGmTunmU8BFOEDL5JLt0vKVmDtQYb7KLGs2qWD1/2i7OlKCH0kBh9oGA4EXWnEjKgyCxRuSlcf1t84EG0+RU/XMF7KrZY8eFLXzjn3c=" /> </div>
    <div>
        <?php include 'header.php';?>
        <div id="myButton"></div>
    </div>
    <div class="page-banner"> <img src="assets/images/sub1.jpg" alt="sub-banner" width="100%" class="sub-banner" />
        <h1 class="h1-banner">
            PRE PRIMARY SCHOOL PROGRAM</h1> </div>
    <div class="page">
        <div class="container">
            <div class="m-t-100"> <img src="assets/images/unicorn.png" alt="unicornlogo" />
                <p class="p-t-10"> The Pre-Primary School Program at Indo - British School comprises Nursery, Lower and Upper Kindergarten (LKG and UKG).</p>
                <p class="p-t-10"> Early childhood education serves as the foundation for all future learning. These are the formative years of learning which play vital role in the life a child. The pre-primary curriculum is delivered through the ‘Ecoliere Infinity’ approach based on the following premise:</p>
                <div class="p-t-50 text-center"> <img src="assets/images/primary.jpg" alt="indo-british" /> </div>
                <div class="row p-t-50">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 wow fadeInLeft">
                        <ul class="list-none">
                            <li class="gray-bg adm-list">
                                <div class="circle bgclr1"> <img src="assets/images/d1-8.png" alt="icons" class="" /> </div>
                                <p> Every child is unique</p>
                            </li>
                            <li class="gray-bg adm-list">
                                <div class="circle bgclr2"> <img src="assets/images/d2-8.png" alt="icons" class="" /> </div>
                                <p> Every child has infinite potential</p>
                            </li>
                            <li class="gray-bg adm-list">
                                <div class="circle bgclr3"> <img src="assets/images/d3-8.png" alt="icons" class="" /> </div>
                                <p> Every child is born with an innovate desire to learn</p>
                            </li>
                            <li class="gray-bg adm-list">
                                <div class="circle bgclr4"> <img src="assets/images/d4-8.png" alt="icons" class="" /> </div>
                                <p> Every child learns best through observation</p>
                            </li>
                            <li class="gray-bg adm-list">
                                <div class="circle bgclr5"> <img src="assets/images/d5-8.png" alt="icons" class="" /> </div>
                                <p> Every child learns and constructs her own knowledge in multiple ways</p>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 wow fadeInRight">
                        <p class="text-justify"> The ‘Ecoliere Infinity’ curriculum focuses on the milestones in the area of cognitive, linguistic, creative, social and physical development. The learning design leads a child to inquire and seek answers. Questions become the order of the day causing thinking and creativity to be the norms. A variety of experiential activities are planned for delivering themes based concepts. Curriculum provides numerous exposures to nurture sensory and gross motor skills, and also small muscle and large muscle activities for physical development of children. To cater to educational needs of children, the school has multiple teaching aids like interesting audio – visual aids, interactive white board, various toys and games for cognitive development and a series of teaching material for introducing concept that build strong recognition among children.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="p-t-50">
            <div class="gray-bg wow fadeInDown">
                <div class="p-t-50 p-b-50 container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <div class="white-bg1 text-center padd-20 height500">
                                <h2 class="subtitle">
                                    FREE <span>PLAY</span>
                                </h2>
                                <p class="p-t-20 text-justify"> A day at SCHOOL begins with Free play where the children are allowed to initiate and direct their own learning styles by choosing from a range of learning material and equipments displayed in the four corners of the class rooms. </p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <div class="white-bg1 text-center padd-20 height500">
                                <h2 class="subtitle">
                                    ATTENDANCE AND <span>ASSEMBLY</span>
                                </h2>
                                <p class="p-t-20 text-justify"> This is followed by Attendance and Assembly where the children learn to self-register through activities such as by printing their finger or sticking their picture next to their name. Children also discuss the previous day's activities and teacher tells them about the day's events. </p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <div class="white-bg1 text-center padd-20 height500">
                                <h2 class="subtitle">
                                    CONCEPT TIME <span>(NURSERY) </span>
                                </h2>
                                <p class="p-t-20 text-justify"> These involve activities based in the month's theme and are planned to meet the unique learning styles of the children. The facilitator may sing a rhyme with actions, show charts / flash cards, play a game, take the children for a walk outdoors or do an art activity to introduce and reinforce concepts. The revision will also provide a range of pathways for the children to explore. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="p-t-50 p-b-50 wow rollIn">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="f7-bg border-50"> <img src="assets/images/music-8.png" alt="music-icons" /> </div>
                        <h5 class="text-center p-t-20">
                            Music And Movement Time</h5> </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="f7-bg border-50"> <img src="assets/images/tummy-8.png" alt="music-icons" /> </div>
                        <h5 class="text-center p-t-20">
                            Tummy Time</h5> </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="f7-bg border-50"> <img src="assets/images/play-8.png" alt="music-icons" /> </div>
                        <h5 class="text-center p-t-20">
                            Language & Number Skills</h5> </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="f7-bg border-50"> <img src="assets/images/language-8.png" alt="music-icons" /> </div>
                        <h5 class="text-center p-t-20">
                            Outdoor Play / Sand Play / Water Play</h5> </div>
                </div>
            </div>
        </div>
        <div class="p-b-50">
            <div class="container wow fadeInUp">
                <h2 class="subtitle text-center">
                    ASSESSMENT IN <span>PRE PRIMARY SCHOOL</span></h2>
                <ul class="list-none p-t-20">
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> IBGS Assessment Card is called Milestones</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> ‘Milestones’ is an observation based evaluation system and does not require the children to go through tests and exams.</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Milestones allows for children to develop at their own pace. It is continuous and includes parental inputs and anecdotal evidence.</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Children are assessed individually and the assessment is inclusive of the learning experience and understanding.</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php include 'footer.php';?>
    </div>
    </div>
    <div class="modal" id="modalPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    <h2 class="subtitle text-left">
                        For Admissions</h2>
                    <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
                </div>
                <div class="m-t-20">
                    <label class="form-label"> Name</label>
                    <input name="ctl00$txtName" type="text" id="txtName" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Email</label>
                    <input name="ctl00$TextBox1" type="text" id="TextBox1" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Mobile Number</label>
                    <input name="ctl00$TextBox2" type="text" id="TextBox2" class="form-control" /> </div>
                <div class="m-t-20 text-center">
                    <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include 'footer-scripts.php';?>
</body>

</html>