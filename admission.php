<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="ms-icon-144x144.php" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/slick.css" />
    <link rel="stylesheet" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/notify-bootstrap.css" />
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css" />
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="js/cities.js"></script>
    <script type="text/javascript">
        var onloadCallback = function() {
            alert("grecaptcha is ready!");
        };
    </script>
</head>

<body style="background: none">
<div class="aspNetHidden">
    <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
    <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
    <input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE1NTczNjI3OTYPZBYCZg9kFgICAw9kFgICAQ9kFgYCEw8QDxYGHg1EYXRhVGV4dEZpZWxkBQlDbGFzc05hbWUeDkRhdGFWYWx1ZUZpZWxkBQdDbGFzc0lEHgtfIURhdGFCb3VuZGdkEBUJCi0tU2VsZWN0LS0HR1JBREUgSQhHUkFERSBJSQlHUkFERSBJSUkIR1JBREUgSVYHR1JBREUgVghHUkFERSBWSQpHUkFERSBWSUlJCUdSQURFIFZJSRUJATADNDA0AzQwNQM0MDYDNDA3AzQwOAM0MDkDNDQwAzQ0MRQrAwlnZ2dnZ2dnZ2dkZAIXDxBkEBUmCi0tU2VsZWN0LS0bQW5kYW1hbiBhbmQgTmljb2JhciBJc2xhbmRzDkFuZGhyYSBQcmFkZXNoEUFydW5hY2hhbCBQcmFkZXNoBUFzc2FtBUJpaGFyCkNoYW5kaWdhcmgMQ2hoYXR0aXNnYXJoFkRhZGFyIGFuZCBOYWdhciBIYXZlbGkNRGFtYW4gYW5kIERpdQVEZWxoaQNHb2EHR3VqYXJhdAdIYXJ5YW5hEEhpbWFjaGFsIFByYWRlc2gRSmFtbXUgYW5kIEthc2htaXIJSmhhcmtoYW5kCUthcm5hdGFrYQZLZXJhbGEGTGFkYWtoCkxha3NoYWRlZXAOTWFkaHlhIFByYWRlc2gLTWFoYXJhc2h0cmEHTWFuaXB1cglNZWdoYWxheWEHTWl6b3JhbQhOYWdhbGFuZAZPcmlzc2ELUG9uZGljaGVycnkGUHVuamFiCVJhamFzdGhhbgZTaWtraW0KVGFtaWwgTmFkdQlUZWxhbmdhbmEHVHJpcHVyYQ1VdHRhciBQcmFkZXNoC1V0dGFyYWtoYW5kC1dlc3QgQmVuZ2FsFSYBMAIzMQE2ATcBOAE5AjMyAjEwAjMzAjM0AjM1AjExAjEyATQCMTMCMTQCMTUCMTYBNQMxMjYCMzYCMTcCMTgCMTkCMjACMjECMjICMjMCMzcCMjQBMwIyNQIyNgI4MwIyNwIyOQIyOAIzMBQrAyZnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZxYBZmQCGQ8QZBAVAQotLVNlbGVjdC0tFQEBMBQrAwFnZGRk1nWV4g1hlw+BrAsgRXxi+N1aYx6htOZSOoXoXhNZGSs=" /> </div>
<script type="text/javascript">
    //<![CDATA[
    var theForm = document.forms['form1'];
    if(!theForm) {
        theForm = document.form1;
    }

    function __doPostBack(eventTarget, eventArgument) {
        if(!theForm.onsubmit || (theForm.onsubmit() != false)) {
            theForm.__EVENTTARGET.value = eventTarget;
            theForm.__EVENTARGUMENT.value = eventArgument;
            theForm.submit();
        }
    }
    //]]>
</script>
<script src="WebResource0306.js?d=0fTtYMduSihgtOTs1lEPHwrfFnnZFJf9ylrBSBJQ7HQOcENpgx0hr0jvNBH864fbU1td3BfczsBEiBMlo6iB9Q3DGC0uZakt22dOctOnWQ41&amp;t=637103544965614113" type="text/javascript"></script>
<script src="WebResource2a08.js?d=OjLE8FJIdkLylHpf6Cpt4JX-a6u38CCHxNh-PQtMrtE_5SRZpeN7xC3UYlQNa27C7_vaQAXCCO86soExmttA66Gk1wdN03Qfitee6sn-xhs1&amp;t=637103544965614113" type="text/javascript"></script>
<script type="text/javascript">
    //<![CDATA[
    function WebForm_OnSubmit() {
        if(typeof(ValidatorOnSubmit) == "function" && ValidatorOnSubmit() == false) return false;
        return true;
    }
    //]]>
</script>
<div class="aspNetHidden">
    <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="68116AE9" />
    <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAELO5Ny5ZfsjrvU7Gc2hZBylITAC/yfRjqZIixmnZPRfi8AvAbS5q9E9h9gTubhs6pyqQLpX+aV+Mguiu0v7wneamuNF/9cirfdCjNTMcEOT1s3ldVuunTLxEA1C2gXlzJBo+TLCDEZvBxKG1SD4G82tu9uq5kgmWpNhxh4MZHqY4MT+paE17ppT9HHFjBUheSqcUOdB9G8tOhBTsAxljKH/m/N/GvlX2rKb/6kICqkFehJMpZ0U9FzelTuqGGM9/AhIpVu8UEiuugR7DguIa3+SGcwa4k9v50kHfH5eBtvvx7k/QA5XKHLbosbontvbEIeJ7W+zNQ4UOIVZ5vEdQFrEVYvvDE3B/nbFSw/zkgYy0leEV9yVu1vMZTF512LuFtnhG+LniOnZPEm9/5E3QZbLpJTXAjFR8bCLZpMwUz1vL/Kvtut/UsQMrPaac0s4rzKksWFWCXauLpoQAfydcjdAq6SekS7N3QwjuxGFpgDwqhaDYZgFMQ8WgQh+DXypAkLuDKFpKinj/K292bDeDkxhHtVWeyXrEIVmOMO5DeKdD3zevhdTFcb5+BTyhxqi6dhT4WJ8qfHEQrvbwrR3LEnkkpNuhwSaJjD8HZx6aTY8zqH3s2QF6L/S6K0KI40y4AE+CG0U36FqB488E9UlUcn4/fAdcOa+WwDeEOzirCRxv8JIaFuZwYaYhGL+wACCNBtHDhZrO1tB0TXYibfZzEnAe/FcrtiFfMweEucLQ6sWQ1czI3EqPfbisrAyHNVtHZQeFJNFmT0Sb6aKH1lMPf2TJSwlkaE+rRrBsQCKzHS6c7EHND66R+QLpINw0kREV+m+joFB0UIFpq6qtO9TfxXg/VdzoPI7sM6dvrN+FO7vo09L/kr9EaCHGUvrDKncmBFAtMNMuT7dV4rYdc3/wsp2j/wLJXGDzW28S+Ep8SsQc5KDe2+78uktmwZBBZLU1NLYlRZ4Jy9M+Y4G8hTsxjHdgMG9c3+SwLfJh/n/sY02XBHAXlm8ZNEwSaINfpyEG2zNEWdgnF7YalVzDq3bFx5Ph8rJCjGrxl650MIdBTUdbBhoncS7VrmyplDoVFHuyENJOSSIZedcL78/4hxFnU0CPFAlO2/1PcjQz9Niv6bEF0IkSRItbdGkNh2m1gV851ORhCQRbmle6MN1ERC77TwPkCBS1BXm1w7BtYtLk0rfnZbarTh8wzA0i2cd8z6eaO9Q0/+M7Cr8W06fYV4Qfl8ntgSnabTiUgHNpPTrOQ2IZUOtB8+dQENZ55Q5K3DYqpoTtMiPCiy2MjI9yWbK1c7OGmw995na1qNdv3dLYWpOQqtXOPlVmsUInmDdFID047K3QaqoaZO6eZTwEU4QMvkku3S8pWYO1BhvsosazapYPX/aLs6UoIfSQGH2gYDgRdYxCOLCPJ8nGx/cAvgOx/EHzGnAbajZ8mxOQB3TvKCZfw==" /> </div>
<div>
    <?php include 'header.php';?>
    <div id="myButton"></div>
</div>
<div class="page-banner"> <img src="assets/images/sub1.jpg" alt="sub-banner" width="100%" class="sub-banner" />
    <h1 class="h1-banner">
        ADMISSION FORM</h1> </div>
<div class="page">
    <div class="container">
        <div class="m-t-100 m-b-100">
            <div class="col-md-12 wow fadeInUp">
                <form method="POST" id="admission_form" >
                    <div class="m-t-20 row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="form-label"> Student Name <span class="star">* </span></label>
                            <input name="admission_student_name" type="text" id="admission_student_name" class="form-control" Placeholder="Enter Your Name" onfocus="this.placeholder = &#39;&#39;" /> </div>
                    </div>
                    <div class="row  m-t-20">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label class="form-label">Date Of Birth</label>
                            <input name="admission_student_dob" id="admission_student_dob" class="form-control" type="date" /> </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label class="form-label">Mobile Number <span class="star">* </span></label>
                            <input name="admission_student_mobile" type="number" min="10" max="12" id="admission_student_mobile" class="form-control" Placeholder="Enter Your Name" onfocus="" /> <span id="" class="" style="color:Red;visibility:hidden;">*</span> <span id="" title="Enter Valided Mobile Number" class="form-validation" style="color:Red;display:none;">*</span> </div>
                    </div>
                    <div class="m-t-20 row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="form-label"> Parent Name</label>
                            <input name="admission_student_parent_name" type="text" id="admission_student_parent_name" class="form-control" Placeholder="Enter Your Parent Name" onfocus="this.placeholder = &#39;&#39;" /> </div>
                    </div>
                    <div class="m-t-20 row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="form-label"> Email ID <span class="star">* </span></label>
                            <input name="admission_student_email" type="text" id="admission_student_email" tabindex="5" class="form-control" /> <span id="" class="form-control" style="color:Red;visibility:hidden;">*</span> <span id="" title="Enter Valided Email Address" class="" style="color:Red;display:none;">*</span> </div>
                        <p id="email_eror" style="color: red"></p>
                    </div>
                    <div class="row  m-t-20">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label class="form-label">Admission For Which Class</label>
                            <select name="admission_for_class" id="admission_for_class" class="form-control">
                                <option selected="selected" value="0">--Select--</option>
                                <option value="GRADE I">GRADE I</option>
                                <option value="GRADE II">GRADE II</option>
                                <option value="GRADE III">GRADE III</option>
                                <option value="GRADE IV">GRADE IV</option>
                                <option value="GRADE V">GRADE V</option>
                                <option value="GRADE VI">GRADE VI</option>
                                <option value="GRADE VIII">GRADE VIII</option>
                                <option value="GRADE VII">GRADE VII</option>

                            </select>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label class="form-label">Admission for which Year</label>
                            <select name="admission_for_year" id="admission_for_year" class="form-control">
                                <option value="0">--Select Admission Year--</option>
                                <option value="1">2022-2023</option>
                            </select>
                        </div>
                    </div>
                    <div class="row  m-t-20">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label class="form-label">State</label>
                            <select onchange="print_city('state', this.selectedIndex);" id="sts" name ="stt" class="form-control"></select>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label class="form-label">City</label>
                            <select name="" id="state" class="form-control admission_for_city">
                                <option value="0">--Select--</option>
                            </select>
                        </div>
                    </div>
                    <div class="m-t-20 row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="form-label">Source</label>
                            <select name="admission_for_source" id="admission_for_source" class="form-control">
                                <option value="0">--Select Source--</option>
                                <option value="1">Sibling Studying in the school </option>
                                <option value="2">Friends / Neighbours</option>
                                <option value="3">Hoarding</option>
                                <option value="4">Newspaper & Magazine</option>
                                <option value="4">Website</option>
                            </select>
                        </div>
                    </div>
                    <div class="m-t-20" style="padding-left:15px">
                        <div class="g-recaptcha" data-sitekey="<?=$capchaSiteKey?>"></div>
                    </div>
                    <div class="m-t-20">
                        <div id="thankyoumessage" style="color: green"></div>
                        <div class="col-md-12 text-center">
                            <input type="button" name="" value="Submit" id="admission_form_btn" class="btn btn-submit" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php';?>
</div>
</div>
<div class="modal" id="modalPopup">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-head">
                <h2 class="subtitle text-left">
                    For Admissions</h2>
                <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
            </div>
            <div class="m-t-20">
                <label class="form-label"> Name</label>
                <input name="ctl00$txtName" type="text" id="txtName" class="form-control" /> </div>
            <div class="m-t-20">
                <label class="form-label"> Email</label>
                <input name="ctl00$TextBox1" type="text" id="TextBox1" class="form-control" /> </div>
            <div class="m-t-20">
                <label class="form-label"> Mobile Number</label>
                <input name="ctl00$TextBox2" type="text" id="TextBox2" class="form-control" /> </div>
            <div class="m-t-20 text-center">
                <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
            </div>
        </div>
    </div>
</div>
</form>
<script language="javascript">print_state("sts");</script>
<?php include 'footer-scripts.php';?>
<script>

    $(function() {

        $("input#admission_student_name").keyup(function(){
            if ($(this).val()==''){
                $('input#admission_student_name').css("border", "1px solid red");
            }else {
                $('input#admission_student_name').css("border", "1px solid green");
            }
        });

        $("input#admission_student_email").keyup(function(){
            if ($(this).val()==''){
                $('input#admission_student_email').css("border", "1px solid red");
            }else {
                $('input#admission_student_email').css("border", "1px solid green");
            }

            if (isValidEmailAddress($(this).val()) && $(this).val()!=''){
                $('input#admission_student_email').css("border", "1px solid green");
                $('p#email_eror').html("");
            }

        });

        $("input#admission_student_mobile").keyup(function(){
            if ($(this).val()==''){
                $('input#admission_student_mobile').css("border", "1px solid red");
            }else {
                $('input#admission_student_mobile').css("border", "1px solid green");
            }
        });


        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test(emailAddress);
        }

        $('#admission_form_btn').on('click', function(e){

            var baseUrl          ='<?=$baseUrl?>';
            var name                                =$('input#admission_student_name').val();
            var email                               =$('input#admission_student_email').val();
            var mobileno                            =$('input#admission_student_mobile').val();
            var admission_student_dob               =$('input#admission_student_dob').val()||'';
            var admission_student_parent_name        =$('input#admission_student_parent_name').val()||'';
            var admission_for_class                  =$('select#admission_for_class').val()||'';
            var admission_for_year                  =$('select#admission_for_year').val()||'';
            var admission_for_state                 =$('select#sts').val()||'';
            var city                                =$('select#state').val()||'';
            var admission_for_source                =$('select#admission_for_source').val()||'';


            var error=0;

            if (name==''){
                $('input#admission_student_name').css("border", "1px solid red");
                error=1;
            }else {
                $('input#admission_student_name').css("border", "1px solid green");
            }

            if (email==''){
                $('input#admission_student_email').css("border", "1px solid red");
                error=1;
            }else {
                $('input#admission_student_email').css("border", "1px solid green");
            }

            if (mobileno==''){
                $('input#admission_student_mobile').css("border", "1px solid red");
                error=1;
            }else {
                $('input#admission_student_mobile').css("border", "1px solid green");
            }

            if (!isValidEmailAddress(email) && email!=''){
                $('input#admission_student_email').css("border", "1px solid red");
                $('p#email_eror').html("Invalid email address");
            }

            if (error==1){
                return false;
            }

            if (error==0){
                $.ajax({
                    url:baseUrl+'/email/admission_email.php',
                    type: "POST",
                    data: {
                        name  :name,
                        email: email,
                        mobileno: mobileno,
                        admission_student_dob: admission_student_dob,
                        admission_student_parent_name: admission_student_parent_name,
                        admission_for_class: admission_for_class,
                        admission_for_year: admission_for_year,
                        admission_for_state: admission_for_state,
                        city: city,
                        admission_for_source: admission_for_source,
                    },
                    success: function(data){
                        if (data==1){
                            $('div#thankyoumessage').html("<h2>Thank you for writing to us. Our representative will soon contact you and address your queries shortly</h2>");
                        }
                    },
                    error: function(){
                        alert("Something went wrong")
                    }
                });
            }
        });
    });
</script>
</body>

</html>