<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/slick.css" />
    <link rel="stylesheet" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/notify-bootstrap.css" />
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css" />
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
</head>

<body style="background: none">
<form method="post" action="./entry-criteria.php" id="form1">
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTY1NDU2MTA1MmRkz2zulVV+FaCyIX2Uu2xVcpfRE7Z0ZRJ3PWqpFsuBtYg=" /> </div>
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="847D8FA4" />
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAASRTMD7fWRdJQ3r+Lsg1BGKt0GqqGmTunmU8BFOEDL5JLt0vKVmDtQYb7KLGs2qWD1/2i7OlKCH0kBh9oGA4EXWGpB9LFlIDzkdT2Vt7xHtWMxcXPXnZkemCfKd28FBCl0=" /> </div>
    <div>
        <?php include 'header.php';?>
        <div id="myButton"></div>
    </div>
    <div class="page-banner"> <img src="assets/images/sub1.jpg" alt="sub-banner" width="100%" class="sub-banner" />
        <h1 class="h1-banner">
            ENTRY CRITERIA FOR ADMISSION</h1> </div>
    <div class="page">
        <div class="container">
            <div class="m-t-100 m-b-100">
                <h2 class="subtitle">
                    ENTRY CRITERIA <span>FOR ADMISSION </span>
                </h2>
                <ul class="list-none wow fadeInLeft">
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Admission is to be granted to student on the basis of level rediness to check the fitness into the Indo-Brish Global School System.</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Student viva to be considered only as an interface tool ll class VII. It has no weight in the final selection.</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Concept check shall be based on expected key knowledge for every class.</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Observation and viva will be conducted in person. Students need to be present for this.</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> Student of Indo Brish School will Not have any entrance test assessment in case of internal transfer.</p>
                    </li>
                    <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                        <p> No Admission test ll Class II.</p>
                    </li>
                </ul>
                <p> Note: Assessments are only meant to determine support requirements of the child. it is not a criterion for rejection of admission. </p>
                <div class="p-t-20">
                    <h2 class="subtitle">
                        ADMISSION <span>PREFERENCES </span>
                    </h2>
                    <ul class="list-none wow fadeInRight">
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Admission to schools in metros will be granted on the basis of prevailing norms of the regulatory authority.</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Unicorn kids students will be granted admission to Indo-Brish Global Schools on a priority basis.</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> The neighborhood residence parameter shall be considered only if it is a norm by the local regulatory authority.</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Admission preference shall be extended to the students’ siblings. However, the minimum admission criteria will be applicable.</p>
                        </li>
                    </ul>
                    <p class="p-t-20 wow fadeInUp"> Conditions to the above rule: If the sibling has any learning disability, the candidate shall be given admission preference depending upon the availability of support system. However, in case of physical disability, admissions shall be granted subject to management's discretion. </p>
                </div>
                <div class="p-t-20 wow rollIn">
                    <h2 class="subtitle">
                        ADMISSION FOR STUDENTS <span>WITH SPECIAL NEEDS</span>
                    </h2>
                    <p class="p-t-20"> Admissions of children with special needs shall be routed through the Head of Child Development & Academics, Vashvik Shodh Pvt. Ltd.This decision shall be considered final. The decision shall be based on facilies and services that can be offered by Indo-Brish Global School. </p>
                    <p class="p-t-20"> Indo-Brish Global School students with idenfied learning disabilies shall be provided support in the form of a resource centre or advisory services</p>
                </div>
                <div class="p-t-20 wow fadeInLeft">
                    <h2 class="subtitle">CONDITIONS FOR <span>REFUSAL OF ADMISSION</span> </h2>
                    <p class="p-t-20">Admissions can be refused only on the basis of non-availability of seats (35 students per batch of a class)</p>
                    <p class="p-t-10">Admission can also be refused on special needs ground, which is subject to approval of refusal reasons by VSPL head office.</p>
                    <p class="p-t-10">Admissions will be granted on first-come-first-serve basis ll school reaches full capacity. Thereaer, admission shall be granted based on a draw of lots. </p>
                </div>
                <div class="p-t-20">
                    <h2 class="subtitle">DOCUMENTS TO BE SUBMITTED AT <span> THE TIME OF ADMISSION </span></h2>
                    <ul class="list-none p-t-20 wow fadeInRight">
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p>Medical fitness cerficate</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p>Attested copy of birth cerficate</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p>Progress card of previous academic year (Std. I onwards)</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p>Previous school’s leaving cerficate, If any</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p>Ration card or passport copy or aadhar card </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php include 'footer.php';?>
    </div>
    </div>
    <div class="modal" id="modalPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    <h2 class="subtitle text-left">
                        For Admissions</h2>
                    <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
                </div>
                <div class="m-t-20">
                    <label class="form-label"> Name</label>
                    <input name="ctl00$txtName" type="text" id="txtName" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Email</label>
                    <input name="ctl00$TextBox1" type="text" id="TextBox1" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Mobile Number</label>
                    <input name="ctl00$TextBox2" type="text" id="TextBox2" class="form-control" /> </div>
                <div class="m-t-20 text-center">
                    <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include 'footer-scripts.php';?>
</body>

</html>