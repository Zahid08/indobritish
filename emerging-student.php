<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/slick.css" />
    <link rel="stylesheet" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/notify-bootstrap.css" />
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css" />
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
</head>

<body style="background: none">
<form method="post" action="#" id="form1">
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTY1NDU2MTA1MmRkgwIeCE0NoqMdoUPjgVZnK4/9S6j3ZuInj4uni1h+Kc4=" /> </div>
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BE3AF717" />
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAQsSmK/run/9CFyRo7t3awat0GqqGmTunmU8BFOEDL5JLt0vKVmDtQYb7KLGs2qWD1/2i7OlKCH0kBh9oGA4EXWtq9u1pqwsn9a/ZCdnM+LUgJLSPxuUQovT0DHTssKwJA=" /> </div>
    <div>
        <?php include 'header.php';?>
        <div id="myButton"></div>
    </div>
    <div class="page-banner"> <img src="assets/images/sub1.jpg" alt="sub-banner" width="100%" class="sub-banner" />
        <h1 class="h1-banner text-uppercase">
            Ecoliere Infinity</h1> </div>
    <div class="m-t-100 m-b-100 page-content">
        <div class="container"> <img src="assets/images/emerging-student.jpg" width="100%" class="wow rollIn" />
            <div class="p-t-50">
                <p> Ecoliere Infinity Provide learners with infinite possibilities and perspectives to learn with experiential learning in collaborative and open minded environment. Ecoliere Infinity Empower learners with citizenship principles, Global/Digital & Media/ Financial knowledge in Healthy & safety / environment. Ecolier Infinity Strengthen learners with transfer of Life Skills and the skills of Learning to Learn. Ecolier Infinity Nurture a culture of learning by involving parents, teachers, community and the society, who act as role models and facilitate the process of children becoming aware, responsible happy, mindful and future ready global citizens. </p>
                <div class="wow fadeInUp">
                    <h2 class="subtitle text-center p-t-50">
                        ECOLIER INFINITY <span>LEARNING APPROACH (EILA)</span>
                    </h2>
                    <p class="p-t-20"> EILA is a future ready approach.</p>
                    <ul class="p-t-10 list-none">
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Places the child at the centre of learning</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Addresses learning styles and needs of a child</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Acknowledges the significance of learning process as much as its product/ outcome Focusses on the why, how, what and when of every concept/ principle being learnt</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Aims at transfer of skills (learning and 21st century life skills), attitude and values besides knowledge </p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Empowers teachers to be imaginative, creative, innovative and empathetic </p>
                        </li>
                    </ul>
                </div>
                <div class="p-t-20 wow fadeInLeft">
                    <div class="box-modal">
                        <h4 class="subtitle">
                            EILA Content</h4>
                        <p class="m-t-10"> The curriculum has been designed with best global practices, knowledge and skills. The global curriculum has been embedded in our content to ensure the IBGS learner to be a Happy Global Citizen. </p>
                        <p class="m-t-10"> Blended Learning experience through integrating technology & academics </p>
                        <p class="m-t-10"> Effective use of e-content, teaching aids to ensure the experiential learning which has been followed by select schools globally</p>
                    </div>
                </div>
                <div class="p-t-20 wow fadeInRight">
                    <div class="box-modal">
                        <h4 class="subtitle">
                            EILA Educators</h4>
                        <p class="m-t-10"> Rigorous and comprehensive hiring and training processes for educators to keep them abreast of the global teaching and learning approach i.e., Ecolier Infinity Learning Approach </p>
                        <p class="m-t-10"> EILA experience through appreciave inquiry, interaction and immersion. </p>
                        <p class="m-t-10"> Teaching Excellence by deploying innovave and futurisc pedagogies to augment learning experience and outcomes. </p>
                        <p class="m-t-10"> Educators undergo 360-degree assessments to ensure that student gets the best learning environment.</p>
                        <p class="m-t-10"> Teaching learning tools enables educators to deliver an experiential learning experience to the students. </p>
                    </div>
                </div>
                <div class="p-t-20 wow fadeInLeft">
                    <div class="box-modal">
                        <h4 class="subtitle">
                            EILA Assessments
                        </h4>
                        <p class="m-t-10"> Learner analytics Track’s students’ journey and predicts outcome. </p>
                        <p class="m-t-10"> The Assessment is For, As and Of learning with differentiation for inclusion. </p>
                        <p class="m-t-10"> Tracking the progress, interests, strengths and areas of improvement through school years to inform Career Choices. </p>
                    </div>
                </div>
                <div class="p-t-20 wow fadeInRight">
                    <div class="box-modal">
                        <h4 class="subtitle">
                            EILA Parents</h4>
                        <p class="m-t-10"> Parents become critical stakeholders in the entire learning process so oriented through ‘Parents Along with Child and Teachers’ (PACT) safety training and various workshops to become effective EILA Parents. </p>
                    </div>
                </div>
                <div class="p-t-20 wow fadeInLeft">
                    <div class="box-modal">
                        <h4 class="subtitle">
                            EILA Enrichment</h4>
                        <p class="m-t-10"> EILA believes in balanced, happy, global citizen and enrichment is very crucial element for child’s holistic growth, health and well-being. </p>
                        <p class="m-t-10"> Dance, Music, Sports, Adventure trips, field visits, student exchange programs are few of many enrichment activities conducted in IBGS. </p>
                    </div>
                </div>
                <div class="p-t-20 wow fadeInRight">
                    <div class="box-modal">
                        <h4 class="subtitle">
                            EILA Literacy & Life skills</h4>
                        <p class="m-t-10"> IBGS Programme has added essential and unique Literacies and Life Skills along with STREAM where STREAM is an integrated approach to learning which requires an intentional connection between standards, assessments and lesson design/implementation. True STREAM experiences involve two or more standards from Science, Technology, Engineering, Math and the Arts to be taught AND assessed in and through each other with in-depth Research as one of the crucial elements. Hence I have for the first time added an “R” in STEAM to make it STREAM as one of the USPs of Ecoliere Curriculum </p>
                    </div>
                </div>
                <div class="p-t-20 wow fadeInLeft">
                    <div class="box-modal">
                        <h4 class="subtitle">
                            EILA Infrastructure</h4>
                        <p class="m-t-10"> State-of-the-art Indo-British Global School design ensures positive and engaging environment for learning indoor and outdoor. Building As Learning Aid (BALA) is embedded in every IBGS architectural design. </p>
                        <p class="m-t-10"> Effective use of technology for developing 21st century global citizens (e-indobritish, teaching aids for experiential learning, etc). </p>
                        <p class="m-t-10"> Technology to learner analytics for tracking students’ journey, connect all stakeholders in the students’ development – Educators, Parents and students. </p>
                    </div>
                </div>
                <div class="p-t-20 wow fadeInRight">
                    <div class="box-modal">
                        <h4 class="subtitle">
                            EILA Network</h4>
                        <p class="m-t-10"> Networking among students, educators and parents across the globe. </p>
                        <p class="m-t-10"> Facilitates sharing of knowledge, ideas, views thereby adding enriched perspectives to the learning process. </p>
                        <p class="m-t-10"> Indo-British Olympiad helps students to compete nationally. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'footer.php';?>
    </div>
    </div>
    <div class="modal" id="modalPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    <h2 class="subtitle text-left">
                        For Admissions</h2>
                    <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
                </div>
                <div class="m-t-20">
                    <label class="form-label"> Name</label>
                    <input name="ctl00$txtName" type="text" id="txtName" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Email</label>
                    <input name="ctl00$TextBox1" type="text" id="TextBox1" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Mobile Number</label>
                    <input name="ctl00$TextBox2" type="text" id="TextBox2" class="form-control" /> </div>
                <div class="m-t-20 text-center">
                    <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include 'footer-scripts.php';?>
</body>

</html>