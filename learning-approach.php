<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/slick.css" />
    <link rel="stylesheet" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/notify-bootstrap.css" />
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css" />
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
</head>

<body style="background: none">
<form method="post" action="#" id="form1">
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTY1NDU2MTA1MmRkWarZESnhRzWnp2DcV1GLcBSxw0vmy7XmSxEJoYvrPd0=" /> </div>
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C380CE9F" />
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAQtFGFD/ox98hRp6ZsWy4met0GqqGmTunmU8BFOEDL5JLt0vKVmDtQYb7KLGs2qWD1/2i7OlKCH0kBh9oGA4EXWmqkdNRs5gBsQuux4R+PPgSAiaalkJgoQMKi0QQtPgi4=" /> </div>
    <div>
        <?php include 'header.php';?>
        <div id="myButton"></div>
    </div>
    <div class="page-banner"> <img src="assets/images/sub1.jpg" alt="sub-banner" width="100%" class="sub-banner" />
        <h1 class="h1-banner">
            LEARNING APPROACH</h1> </div>
    <div class="page">
        <div class="container">
            <div class="m-t-100 m-b-100">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <h2 class="subtitle">LEARNING <span>OUTCOMES</span></h2> <img src="assets/images/learning.png" alt="learning-outcomes" />
                        <div class="m-t-100">
                            <h2 class="subtitle">21 CENTURY SKILLS & <span> HABITS OF MIND</span></h2>
                            <div class="row p-t-20">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 wow rollIn">
                                    <ul class="list-none">
                                        <li>Critical Thinking, Problem Solving</li>
                                        <li>Creativity & Innovation</li>
                                        <li>Collaboration</li>
                                        <li>Communication</li>
                                        <li>Information Literacy</li>
                                        <li>Media Literacy</li>
                                    </ul>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 wow rollIn">
                                    <ul class="list-none">
                                        <li>Technology Literacy</li>
                                        <li>Flexibility</li>
                                        <li>Leadership</li>
                                        <li>Initiative</li>
                                        <li>Productivity</li>
                                        <li>Social Skills</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 wow fadeInLeft">
                        <div class="back-bg">
                            <h2 class="clr-fff pos-relative">ESP ATTRIBUTES</h2>
                            <ul class="list-none p-t-20 pos-relative clr-fff">
                                <li class="clr-fff">CURIOUS</li>
                                <li class="clr-fff">ACTION DRIVEN</li>
                                <li class="clr-fff">REFLECTIVE</li>
                                <li class="clr-fff">HAPPY</li>
                                <li class="clr-fff">RESILIENT</li>
                                <li class="clr-fff">CONSCIENTIOUS</li>
                                <li class="clr-fff">LEADER</li>
                            </ul>
                        </div>
                        <div class="m-t-100">
                            <h2 class="subtitle">PEDAGOGY: TEACHING - <span> LEARNING METHODS AND STRATEGIES</span></h2>
                            <div class="row p-t-20">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12  wow fadeInRight">
                                    <ul class="list-none">
                                        <li>Constructivism</li>
                                        <li>Inquiry Based Learning</li>
                                        <li>Problem Based Learning</li>
                                        <li>Project Based Learning</li>
                                        <li>Scaffolding</li>
                                        <li>Classroom discussion</li>
                                        <li>Quiz</li>
                                    </ul>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 wow fadeInLeft">
                                    <ul class="list-none">
                                        <li>Peer learning</li>
                                        <li>Role Play</li>
                                        <li>Presentation by individual/ team</li>
                                        <li>Flipped Classroom</li>
                                        <li>Demonstration/Experimentation</li>
                                        <li>Collaborative Learning in Small Groups</li>
                                        <li>Experiential Learning</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-t-100">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 wow fadeInRight">
                            <h2 class="subtitle">DIMENSIONS OF <span> PHILOSOPHY</span></h2>
                            <ul class="p-t-20 list-none">
                                <li>Head/Knowledge/ Literacies/ Learning Skills/ IQ/ Pillar - To Know</li>
                                <li>Heart/ Values & Mindset/ ESQ, HQ, SQ/ Pillar – To Be, To Live Together</li>
                                <li>Hands/ Taking Acon/ Kinaesthec/ Pillar – To Do</li>
                                <div class="p-t-20"> <img src="assets/images/philosophy.png" alt="philosophy" /> </div>
                            </ul>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 wow fadeInUp">
                            <h2 class="subtitle">LEARNING <span>STYLES</span></h2>
                            <ul class="list-none p-t-20">
                                <li>Auditory</li>
                                <li>Visual</li>
                                <li>Kinaesthetic</li>
                            </ul>
                            <h2 class="subtitle">INTERDISCIPLINARY <span> CONNECTIONS</span></h2>
                            <h2 class="subtitle">CAMBRIDGE <span>EXTENSION </span> </h2> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'footer.php';?>
    </div>
    </div>
    <div class="modal" id="modalPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    <h2 class="subtitle text-left">
                        For Admissions</h2>
                    <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
                </div>
                <div class="m-t-20">
                    <label class="form-label"> Name</label>
                    <input name="ctl00$txtName" type="text" id="txtName" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Email</label>
                    <input name="ctl00$TextBox1" type="text" id="TextBox1" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Mobile Number</label>
                    <input name="ctl00$TextBox2" type="text" id="TextBox2" class="form-control" /> </div>
                <div class="m-t-20 text-center">
                    <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include 'footer-scripts.php';?>
</body>

</html>