<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="content-type" content="image/svg+xml" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/shCore.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/shCoreDefault.css" />
    <link rel="stylesheet" href="assets/css/flexslider.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/animate.css" type="text/css" />
    <script type="text/javascript" src="assets/js/modernizr.js"></script>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')
    </script>
    <!-- Wistia iframe API -->
    <script type="text/javascript" src="assets/js/iframe-api-v1.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script type="text/javascript">
        var onloadCallback = function() {
            alert("grecaptcha is ready!");
        };

        function validationNumeric(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if(charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else {
                return true;
            }
        }
    </script>
</head>

<body>
<div class="aspNetHidden">
    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE3MTQxNDY1NDJkZJBzz9c3YO0Wjsy3CbkJyEpv+Q7Ouzz/sccp8re0aeBj" /> </div>
<div class="aspNetHidden">
    <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="90059987" />
    <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAn35yh8r9e6hPu3gcRmYAhbVB2oyKqwXwPf0FvAU1V2hVf1tIAwDpA/p3iQAE0sdDvZ9ub/ht1mcRKVTMtNpm3TMxLqjbabUsyRU5RncJq0W6tXOPlVmsUInmDdFID047JV6zgB82sTBlDbnAWaeAefiQePsqwZBn0oj+rRyBHv15FzIkL/tSG/ylS5PBFydkri5EkId+jIcCCRDPZgRYnftt9fr0DOUwcvcH5236PYCQ==" /> </div>
<div>
    <?php include 'header.php';?>
    <div class="wrappper" style="position: relative">
        <section class="slider" style="position: relative">
            <div class="row" style="margin-right: 0px; margin-left: 0px">
                <div class="col-xl-9 col-lg-8 col-md-12 col-sm-12 col-12" style="padding-left: 0px;
                    padding-right: 0px">
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <iframe id="player_1" src="https://indo-british.com/assets/images/IndoBritish%20video%20.mp4" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" width="100%" height="670px" allowfullscreen sandbox="allow-scripts allow-presentation allow-same-origin" allow="fullscreen; picture-in-picture; xr-spatial-tracking; encrypted-media"> </iframe>
                            </li>
                            <li> <img src="assets/images/s1.jpg" alt="slider2" width="100%">
                                <div class="text-box">
                                    <h2 class="wow slideInUp h2-style" data-wow-duration="4s">
                                        ADMISSION OPEN FOR 2021 - 2022</h2>
                                    <p class="wow fadeInDown" data-wow-duration="4s"> INDO- BRITISH Global School believes in rich Indian values and the futuristic British Vision </p>
                                </div>
                            </li>
                            <li> <img src="assets/images/ss2.jpg" alt="slider3" width="100%">
                                <div class="text-box" style="padding: 20px">
                                    <h2 class="wow fadeInUp h2-style" data-wow-duration="4s">
                                        ADMISSION OPEN FOR 2021 - 2022</h2>
                                    <p class="wow fadeInUp" data-wow-duration="2s"> INDO- BRITISH Global School believes in rich Indian values and the futuristic British Vision </p>
                                </div>
                            </li>
                            <li> <img src="assets/images/s2.jpg" alt="slider3" width="100%">
                                <div class="text-box">
                                    <h2 class="wow fadeInUp h2-style" data-wow-duration="4s">
                                        India’s First School chain with Integrated Global Curriculum</h2>
                                    <p class="wow fadeInUp" data-wow-duration="2s"> INDO- BRITISH Global School believes in rich Indian values and the futuristic British Vision </p>
                                </div>
                            </li>
                            <li> <img src="assets/images/s4.jpg" alt="slider3" width="100%">
                                <div class="text-box"> <img src="assets/images/s41.png" alt="slider2" class="wow fadeInUp" data-wow-duration="2s" /> </div>
                            </li>
                            <li> <img src="assets/images/s5.jpg" alt="slider3" width="100%">
                                <div class="text-box"> <img src="assets/images/s51.png" alt="slider2" class="wow fadeInLeft" data-wow-duration="2s" /> </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-12 col-sm-12 col-12" style="padding-left: 0px;
                    padding-right: 0px">
                    <div class="white-bg padd-30">
                        <form method="POST" id="enquiry_form_submit" >
                            <div class="col-sm-12">
                                <h3 class="text-center clr-blu">
                                    Enquire Now</h3>
                                <div class="p-t-10">
                                    <label class="form-label"> Name <span class="star">* </span> </label>
                                    <input name="enquery_name" type="text" id="enquery_name" class="form-control" placeholder="Enter Your Name" onfocus="this.placeholder=&#39;&#39;" /> </div>
                                <div class="p-t-10">
                                    <label class="form-label"> Email <span class="star">* </span> </label>
                                    <input name="enquery_email" type="text" id="enquery_email" class="form-control" placeholder="Enter Your Email Id" onfocus="this.placeholder=&#39;&#39;" /> </div>
                                <p id="email_eror" style="color: red"></p>
                                <div class="p-t-10">
                                    <label class="form-label"> Mobile Number <span class="star">* </span> </label>
                                    <input name="enquery_mobile_no" type="number" id="enquery_mobile_no" class="form-control" placeholder="Enter Your Mobile Number" onkeypress="return validationNumeric(this);" onfocus="this.placeholder=&#39;&#39;" min="10" max="12"/> </div>
                                <div class="p-t-10">
                                    <label class="form-label"> Investment Amount <span class="star">* </span> </label>
                                    <input name="enquery_invest_amount" type="text" id="enquery_invest_amount" class="form-control" placeholder="Enter Your Investment Amount" onkeypress="return validationNumeric(this);" onfocus="this.placeholder=&#39;&#39;" /> </div>
                                <div class="m-t-20" style="padding-left: 0px !important">
                                    <div class="g-recaptcha" data-sitekey="<?=$capchaSiteKey?>"> </div>
                                    <div class="m-t-20">
                                        <div class="col-md-12 text-center">
                                            <input type="button" name="enquery_form_submit" value="Submit" id="enquery_form_submit" class="btn btn-submit" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-xl-9 col-lg-7 col-md-12 col-sm-12 col-12">
                                <div class="m-t-20"> <a href="admission.php" class="btn btn-blue adm">Admission Enquiry <span><i class="fas fa-caret-right">
                                    </i></span></a> </div>
                                <div class="m-t-20"> <a href="https://vaisvikshodh.com/PartnerWithUs" class="btn btn-orange adm">Partner
                                        With Us <span><i class="fas fa-caret-right"></i></span></a> </div>
                            </div>
                            <div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-12">
                                <div id="myButton"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    <section class="m-t-100 m-b-100 wow rollIn">
        <div class="container">
            <div class="text-center">
                <h2 class="h2-title">
                    <span>WELCOME TO </span>
                    <br />
                    INDO-BRITISH GLOBAL SCHOOL</h2> </div>
            <div class="m-t-50">
                <p class="text-center"> The need of an hour is to bring essential changes in the present educational and social structure which should help the individuals towards freedom and integration; The parents are at all in earnest and desire that the child shall grow to his/her fullest integral capacity. At IBGS, we have begun to alter the influence of the home and creating the schools with the right kind of educators. The influence of the home and that of the school complements each other, we educate teachers and parents for the collaboration. We learned and educate to be compassionate, to be content and to seek the supreme. Welcome to Indo-British Global School, a global K-12 school catering to ages 3 to 18 years (Grade Nursery to 12), with an offering of CBSE, IGCSE and ICSE with integrated global curriculum. Indo-British Global School, an initiative of Vaisvik Shodh Pvt. Ltd. brings global curriculum for developing happy and future ready global citizens. </p>
                <div class="p-t-20 text-center"> <a href="about.php" class="orange-btn">Read More</a> </div>
            </div>
        </div>
    </section>
    <div style="float: left; width: 100%; min-height: 645px">
        <div class="light-bg">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="white-bg">
                            <div class="padd-50 text-center">
                                <h2 class="h2-title">
                                    THOUGHT FOR<span>THE DAY</span></h2>
                                <p class="p-t-20"> Intelligence Quotient (IQ) primarily solves problems and designs solutions, products and services that are useful to individuals and communities.</p>
                                <h4 class="h4-style p-t-10" style="font-size: 19px">
                                    ~ Dr. Leena Pimpley (Academic Mentor – VSPL)</h4> </div>
                        </div>
                        <div class="p-t-20">
                            <a href="admission.php" class="blue-bg d-blk">
                                <h2 class="h2-tit">
                                    <span>ADMISSION </span>OPEN</h2>
                                <p class="p-t-20 clr-fff"> Few decisions have the lasting impact on your child as that of your school choice. During these formative years, life-long friendships will be made, spiritual formation will take place, and values will be instilled </p>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12"> <a href="#" class="inl-btn">Plan A Campus Tour</a>
                        <div class="clearfix"> </div>
                        <div class="p-t-20">
                            <div class="ecoliere">
                                <div class="col-xl-12">
                                    <h2 class="h2-tit text-center">
                                        <span>ECOLIERE</span><span class="clr-blue"> INFINITY</span>
                                    </h2>
                                    <p class="clr-fff text-left m-tb-15 letter-spa"> A PHILOSOPHY WITH GLOBAL APPROACH Indo-British Global School has an Integrated Learning approach which gives every child a fair chance to excel his / her mental horizons to the infinity. Nurturing Personal Values : Integrity, Resilience, Excellence, Mindfulness, Caring; Motivating to communicate, collaborate & Lead; Offering Space and Scope for Experimenting, Failing, Retrying, Risk taking, Celebrating efforts & achievements; Modeling & Scaffolding by Adults; Transferring Gamut of Tools for Thinking, Feeling & Taking Action for sustainable existence </p> <a href="emerging-student.php" class="text-center orange-btn">Learn More</a> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"> </div>
    <section class="m-t-100 m-b-100 f-none">
        <div class="container">
            <h2 class="h2-title text-center feature-top">
                Features & <span>Facilities </span>
            </h2>
            <div class="m-t-50 features">
                <div class="row">
                    <div id="sync1" class="slider1 owl-carousel">
                        <div class="item">
                            <div class="frame-bg"> <img src="assets/images/m3.png" alt="phone-images" class="image11" /> </div>
                        </div>
                        <div class="item">
                            <div class="frame-bg"> <img src="assets/images/m2.png" alt="phone-images" class="image11" /> </div>
                        </div>
                        <div class="item">
                            <div class="frame-bg"> <img src="assets/images/m1.png" alt="phone-images" class="image11" /> </div>
                        </div>
                        <div class="item">
                            <div class="frame-bg"> <img src="assets/images/m4.png" alt="phone-images" class="image11" /> </div>
                        </div>
                        <div class="item">
                            <div class="frame-bg"> <img src="assets/images/m3.png" alt="phone-images" class="image11" /> </div>
                        </div>
                        <div class="item">
                            <div class="frame-bg"> <img src="assets/images/m4.png" alt="phone-images" class="image11" /> </div>
                        </div>
                    </div>
                    <div id="sync2" class="navigation-thumbs owl-carousel">
                        <div class="item">
                            <div class="d-fstart">
                                <div class="content">
                                    <h5>
                                        Classrooms For Collabrative Learning</h5>
                                    <p> The classroom seating strategy makes the learning environment vibrant and interactive.</p>
                                </div>
                                <div class="l-circle g-bg">
                                    <h5 class="clr-fff">
                                        <object data="assets/SVG/m1.svg" type="image/svg+xml" style="height: 30px; width: 30px;">
                                        </object>
                                    </h5> </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="d-fstart">
                                <div class="content">
                                    <h5>
                                        Island of knowledge</h5>
                                    <p> The Iceland of knowledge is built age appropriately thus we have a separate library specically designed for pre-primary & primary students. </p>
                                </div>
                                <div class="l-circle dg-bg">
                                    <h5 class="clr-fff">
                                        <object data="assets/SVG/m2.svg" type="image/svg+xml" style="height: 30px; width: 30px;">
                                        </object>
                                    </h5> </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="d-fstart">
                                <div class="content">
                                    <h5>
                                        Discovery Labs</h5>
                                    <p> The best way to learn science is through actions and this is most vibrant place at Indo-British Global School.</p>
                                </div>
                                <div class="l-circle b-bg">
                                    <h5 class="clr-fff">
                                        <object data="assets/SVG/m3.svg" type="image/svg+xml" style="height: 30px; width: 30px;">
                                        </object>
                                    </h5> </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="d-fstart">
                                <div class="r-circle o-bg">
                                    <h5 class="clr-fff">
                                        <object data="assets/SVG/m4.svg" type="image/svg+xml" style="height: 30px; width: 30px;">
                                        </object>
                                    </h5> </div>
                                <div class="content">
                                    <h5>
                                        Smart Learning
                                    </h5>
                                    <p> Adding several different dimensions to mainstream learning is ‘Smart Learning’. </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="d-fstart">
                                <div class="r-circle p-bg">
                                    <h5 class="clr-fff">
                                        <object data="assets/SVG/m5.svg" type="image/svg+xml" style="height: 30px; width: 30px;">
                                        </object>
                                    </h5> </div>
                                <div class="content">
                                    <h5>
                                        Colour Me Happy Studio</h5>
                                    <p> Mainstream learning alone cannot fulfill this crucial need. That is why we have a free-form facility to allow them to dabble in various art forms and crafts. </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="d-fstart">
                                <div class="r-circle pi-bg">
                                    <h5 class="clr-fff">
                                        <object data="assets/SVG/m6.svg" type="image/svg+xml" style="height: 30px; width: 30px;">
                                        </object>
                                    </h5> </div>
                                <div class="content">
                                    <h5>
                                        Crystal Room — Inner Well-being</h5>
                                    <p> a unique facility to strengthen and energize the mind, heart and soul - for greater concentration</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="mobile">
        <div class="col-sm-12 m-t-10">
            <div class="frame"> <img src="assets/images/m3.png" alt="phone-images" class="image11" /> </div>
            <div class="m-t-10">
                <h5>
                    Classrooms For Collabrative Learning</h5>
                <p> The classroom seating strategy makes the learning environment vibrant and interactive.</p>
            </div>
        </div>
        <div class="col-sm-12 m-t-10">
            <div class="frame"> <img src="assets/images/m2.png" alt="phone-images" class="image11" /> </div>
            <div class="m-t-10">
                <h5>
                    Island of knowledge</h5>
                <p> The Iceland of knowledge is built age appropriately thus we have a separate library specically designed for pre-primary & primary students. </p>
            </div>
        </div>
        <div class="col-sm-12 m-t-10">
            <div class="frame"> <img src="assets/images/m1.png" alt="phone-images" class="image11" /> </div>
            <div class="m-t-10">
                <h5>
                    Discovery Labs</h5>
                <p> The best way to learn science is through actions and this is most vibrant place at Indo-British Global School.</p>
            </div>
        </div>
        <div class="col-sm-12 m-t-10">
            <div class="frame"> <img src="assets/images/m4.png" alt="phone-images" class="image11" /> </div>
            <div class="m-t-10">
                <h5>
                    Smart Learning
                </h5>
                <p> Adding several different dimensions to mainstream learning is ‘Smart Learning’. </p>
            </div>
        </div>
        <div class="col-sm-12 m-t-10">
            <div class="frame"> <img src="assets/images/m5.png" alt="phone-images" class="image11" /> </div>
            <div class="m-t-10">
                <h5>
                    Colour Me Happy Studio</h5>
                <p> Mainstream learning alone cannot fulfill this crucial need. That is why we have a free-form facility to allow them to dabble in various art forms and crafts. </p>
            </div>
        </div>
        <div class="col-sm-12 m-t-10">
            <div class="frame"> <img src="assets/images/m6.png" alt="phone-images" class="image11" /> </div>
            <div class="m-t-10">
                <h5>
                    Crystal Room — Inner Well-being</h5>
                <p> a unique facility to strengthen and energize the mind, heart and soul - for greater concentration</p>
            </div>
        </div>
    </section>
    <div class="clearfix"> </div>
    <section class="m-b-100">
        <div class="student-life">
            <div class="width100">
                <div class="width27"> <img src="assets/images/STUDENT-LIFE.jpg" alt="student-life" /> </div>
                <div class="width73">
                    <div class="m-t-50 pos-rel">
                        <h3 class="sub-title">
                            Student life at Indo- British is a rich and
                            <br />
                            vibrant experience.</h3>
                        <p class="p-t-20 clr-fff width80 text-justify"> Striving to provide a well balanced spiritual, academic and social well-being, students are encouraged to both serve and participate in and beyond our community. With plenty of room for opportunity, students can engage in over forty sports, the arts, and equally many student driven clubs. </p>
                        <div class="clearfix p-t-20"> </div> <a href="campus.php" class="p-t-10 orange-btn">Learn More</a>
                        <div class="programs m-t-50 width80 row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <a href="pre-primary.php" class="box-elevation d-block">
                                    <div class="svg-icons">
                                        <!-- Generator: Adobe Illustrator 24.0.2, SVG Export Plug-In  -->
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="74px" height="83.1px" viewbox="0 0 74 83.1" style="enable-background: new 0 0 74 83.1;" xml:space="preserve">
														<style type="text/css">
                                                            .st0 {
                                                                fill: #21BAFE;
                                                            }

                                                            .st1 {
                                                                fill: #085CA7;
                                                            }

                                                            .st2 {
                                                                fill: #F39911;
                                                            }

                                                            .st3 {
                                                                fill: #FFFFFF;
                                                            }
                                                        </style>
                                            <defs> </defs>
                                            <g>
                                                <path class="st0" d="M55.5,37h-27c-2.8,0-5-2.2-5-5V5c0-2.8,2.2-5,5-5h27c2.8,0,5,2.2,5,5v27C60.5,34.8,58.3,37,55.5,37z" />
                                                <path class="st1" d="M32,83.1H5c-2.8,0-5-2.2-5-5v-27c0-2.8,2.2-5,5-5h27c2.8,0,5,2.2,5,5v27C37,80.9,34.8,83.1,32,83.1z" />
                                                <path class="st2" d="M69,73.1H42c-2.8,0-5-2.2-5-5v-27c0-2.8,2.2-5,5-5h27c2.8,0,5,2.2,5,5v27C74,70.9,71.8,73.1,69,73.1z" />
                                                <g>
                                                    <g>
                                                        <path class="st3" d="M46.3,27.8l-0.8-2.7c0-0.1-0.1-0.1-0.1-0.1h-6.7c-0.1,0-0.1,0-0.1,0.1l-0.8,2.7c-0.1,0.2-0.2,0.3-0.4,0.3
				h-3.5c-0.1,0-0.2,0-0.2-0.1c-0.1-0.1-0.1-0.2,0-0.3l5.9-18.5c0.1-0.2,0.2-0.3,0.4-0.3h4.3c0.2,0,0.3,0.1,0.4,0.3l5.9,18.5
				c0,0,0,0.1,0,0.1c0,0.2-0.1,0.2-0.3,0.2h-3.5C46.5,28.1,46.4,28,46.3,27.8z M39.6,22h4.8c0.1,0,0.1-0.1,0.1-0.2L42,13.7
				c0-0.1,0-0.1-0.1-0.1c0,0-0.1,0-0.1,0.1l-2.4,8.1C39.4,22,39.5,22,39.6,22z" /> </g>
                                                </g>
                                                <g>
                                                    <path class="st3" d="M23.8,64.3c2.1,1.1,3.2,2.9,3.2,5.4c0,2.2-0.7,3.8-2.2,4.9c-1.5,1.1-3.4,1.6-5.8,1.6h-8.6
			c-0.1,0-0.2,0-0.3-0.1C10.1,76.1,10,76,10,75.8V53.3c0-0.1,0-0.2,0.1-0.3c0.1-0.1,0.2-0.1,0.3-0.1h8.4c5.3,0,7.9,2.1,7.9,6.4
			c0,2.2-0.9,3.8-2.8,4.8C23.7,64.2,23.7,64.2,23.8,64.3z M14.7,57.1v5.2c0,0.1,0.1,0.2,0.2,0.2h3.9c1,0,1.9-0.2,2.4-0.7
			c0.6-0.5,0.9-1.2,0.9-2c0-0.9-0.3-1.5-0.9-2c-0.6-0.5-1.4-0.7-2.4-0.7h-3.9C14.8,57,14.7,57,14.7,57.1z M21.4,71.4
			c0.6-0.5,0.9-1.2,0.9-2.1c0-0.9-0.3-1.6-0.9-2.1c-0.6-0.5-1.4-0.8-2.4-0.8h-4.1c-0.1,0-0.2,0.1-0.2,0.2V72c0,0.1,0.1,0.2,0.2,0.2
			H19C20.1,72.2,20.9,71.9,21.4,71.4z" /> </g>
                                                <g>
                                                    <path class="st3" d="M50.8,65.9c-1.4-0.7-2.4-1.7-3.1-3c-0.7-1.3-1.1-2.7-1.1-4.4v-8c0-1.7,0.4-3.1,1.1-4.4
			c0.7-1.3,1.8-2.2,3.1-2.9c1.4-0.7,2.9-1,4.7-1c1.8,0,3.3,0.3,4.7,1c1.3,0.7,2.4,1.6,3.1,2.8c0.7,1.2,1.1,2.6,1.1,4.2
			c0,0.2-0.1,0.3-0.4,0.4l-4,0.2h-0.1c-0.2,0-0.3-0.1-0.3-0.4c0-1.2-0.4-2.2-1.1-2.9c-0.7-0.7-1.7-1.1-3-1.1c-1.2,0-2.2,0.4-3,1.1
			c-0.7,0.7-1.1,1.7-1.1,2.9v8.3c0,1.2,0.4,2.2,1.1,2.9c0.7,0.7,1.7,1.1,3,1.1c1.2,0,2.2-0.4,3-1.1c0.7-0.7,1.1-1.7,1.1-2.9
			c0-0.3,0.1-0.4,0.4-0.4l4,0.2c0.1,0,0.2,0,0.3,0.1c0.1,0.1,0.1,0.2,0.1,0.2c0,1.6-0.4,3-1.1,4.2c-0.7,1.2-1.8,2.2-3.1,2.8
			c-1.3,0.7-2.9,1-4.7,1C53.7,67,52.1,66.6,50.8,65.9z" /> </g>
                                            </g>
													</svg>
                                        <h4 class="p-t-10 h4-weight">
                                            Lower School
                                        </h4> </div>
                                </a>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <a href="middle.php" class="box-elevation d-block">
                                    <div class="svg-icons">
                                        <!-- Generator: Adobe Illustrator 24.0.2, SVG Export Plug-In  -->
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="89.1px" height="92px" viewbox="0 0 89.1 92" style="enable-background: new 0 0 89.1 92;" xml:space="preserve">
														<style type="text/css">
                                                            .st10 {
                                                                fill: #21BAFE;
                                                            }

                                                            .st11 {
                                                                fill: #F39911;
                                                            }

                                                            .st12 {
                                                                fill: #085CA7;
                                                            }
                                                        </style>
                                            <defs> </defs>
                                            <g>
                                                <path class="st10" d="M0,11.8c3.2,0.4,6.1,0.7,9,1.2c12.3,2.4,22.9,8.1,32.1,16.6c0.5,0.4,0.8,1.3,0.8,1.9c0,17,0,33.9,0,50.9
		c0,2.6,0,5.2,0,7.8c0,0.4,0,0.9,0,1.7c-1.5-1.2-2.8-2.3-4-3.2c-10.8-8.4-23-13.2-36.7-14.1c-1-0.1-1.3-0.4-1.3-1.3
		c0-19.8,0-39.6,0-59.5C0,13.3,0,12.7,0,11.8z" />
                                                <path class="st11" d="M44.5,89.9c0-2.6,0-4.8,0-7c0-17.1,0-34.2,0-51.3c0-0.8,0.1-1.6,0.5-2.3c4.2-7.5,9-14.6,15.2-20.7
		c3.3-3.3,7-6.1,11-8.4c0.1-0.1,0.3-0.1,0.6-0.3c0,0.5,0.1,1,0.1,1.4c0,19.7,0,39.3,0,59c0,1-0.1,1.8-1.2,2.3
		c-8.8,4.7-15.4,11.7-21.1,19.7c-1.5,2.1-2.8,4.3-4.3,6.4C45.1,89.1,44.9,89.4,44.5,89.9z" />
                                                <path class="st12" d="M88.9,11.7c0.1,0.8,0.1,1.2,0.1,1.5c0,19.9,0,39.8,0,59.7c0,1.3-0.5,1.6-1.6,1.7c-14,0.9-26.6,5.5-38,13.7
		c-0.3,0.2-0.6,0.4-1.1,0.5c0.7-1.1,1.3-2.2,2.1-3.2c4.5-6.4,9.4-12.4,15.8-17c2.1-1.5,4.4-2.9,6.7-4.2c1-0.6,1.4-1.2,1.4-2.4
		c0-15.4,0-30.7,0-46.1c0-1.8,0-1.8,1.8-2.1C80.4,13,84.6,12.4,88.9,11.7z" /> </g>
													</svg>
                                        <h4 class="p-t-10 h4-weight">
                                            Middle School
                                        </h4> </div>
                                </a>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <a href="srsecondary.php" class="box-elevation d-block">
                                    <div class="svg-icons">
                                        <!-- Generator: Adobe Illustrator 24.0.2, SVG Export Plug-In  -->
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100.5px" height="65.4px" viewbox="0 0 100.5 65.4" style="enable-background: new 0 0 100.5 65.4;" xml:space="preserve">
														<style type="text/css">
                                                            .st0 {
                                                                fill: #21BAFE;
                                                            }

                                                            .st1 {
                                                                fill: #085CA7;
                                                            }

                                                            .st2 {
                                                                fill: #F39911;
                                                            }
                                                        </style>
                                            <defs> </defs>
                                            <g>
                                                <path class="st0" d="M7.3,58.6c-0.1-1-0.1-1.8-0.1-2.7c0-16.6,0-33.3,0-49.9c0-4.8,1.3-6,6.1-6c24.7,0,49.3,0,74,0
		c4.7,0,6.1,1.3,6.1,5.8c0,17.5,0,35,0,52.8C64.7,58.6,36.2,58.6,7.3,58.6z M50.2,55c12,0,24.1-0.1,36.1,0c2.4,0,3.2-0.6,3.2-3.1
		c-0.1-14.9-0.1-29.8,0-44.7c0-2.4-0.7-3.2-3.1-3.2c-24.1,0.1-48.2,0.1-72.3,0c-2.3,0-3.1,0.5-3,2.9c0.1,15,0.1,30,0,44.9
		c0,2.3,0.6,3,3,3C26.1,54.9,38.1,55,50.2,55z" />
                                                <path class="st1" d="M100.5,60.5c-0.6,3.7-1.8,4.8-5.3,4.8c-11,0-21.9,0-32.9,0c-18.9,0-37.8,0-56.7,0c-3.8,0-5-0.9-5.7-4.6
		c0.8-0.1,1.7-0.2,2.5-0.2c12.1,0,24.3,0,36.4,0.1c1,0,2,0.9,3.1,1.3c0.9,0.3,1.8,0.8,2.7,0.8c3.7,0.1,7.4,0.1,11.2,0
		c1.1-0.1,2.1-0.9,3.2-1.4c0.6-0.3,1.1-0.7,1.7-0.7C73.9,60.5,87.1,60.5,100.5,60.5z" />
                                                <path class="st2" d="M52.6,29.5c2.4,2.2,4.6,4.2,7,6.4c-1.2,1-2.1,1.7-3.3,2.8c-1.9-2.2-3.8-4.5-6-7c-1.7,2.6-3,4.5-4.6,6.9
		c-1.6-6.2-3-11.8-4.5-18c6.1,1.5,11.7,2.9,18.1,4.5C57,26.6,55.1,27.8,52.6,29.5z" /> </g>
													</svg>
                                        <h4 class="p-t-10 h4-weight">
                                            High School
                                        </h4> </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"> </div>
    <div class="m-t-100">
        <div class="container">
            <h2 class="h2-title text-center">
                <span>LATEST </span>UPDATES</h2>
            <div class="m-t-50">
                <div id="owl-demo" class="owl-carousel owl-theme">
                    <div class="items events">
                        <div class="event-image"> <img src="assets/images/e1.jpg" alt="event-image" width="100%" /> </div>
                        <div class="white-bg event-txt">
                            <h5 class="h5-style">
                                12 OCT 2021</h5> </div>
                    </div>
                    <div class="items events">
                        <div class="event-image"> <img src="assets/images/e2.jpg" alt="event-image" width="100%" /> </div>
                        <div class="white-bg event-txt">
                            <h5 class="h5-style">
                                12 NOV 2021</h5> </div>
                    </div>
                    <div class="items events">
                        <div class="event-image"> <img src="assets/images/e3.jpg" alt="event-image" width="100%" /> </div>
                        <div class="white-bg event-txt">
                            <h5 class="h5-style">
                                12 DEC 2021</h5> </div>
                    </div>
                    <div class="items events">
                        <div class="event-image"> <img src="assets/images/e3.jpg" alt="event-image" width="100%" /> </div>
                        <div class="white-bg event-txt">
                            <h5 class="h5-style">
                                12 OCT 2021</h5> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="p-t-100"> </div>
    <div class="clearfix"> </div>
    <section class="m-t-100">
        <div class="last-section">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                        <div class="news-bg">
                            <div class="p-t-20">
                                <h2 class="h2-title text-center">
                                    News & <span>Events</span></h2> </div>
                            <div class="newstick">
                                <ul class="list-none">
                                    <li class="li-ele d-flex">
                                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3"> <img src="assets/images/n1.png" alt="news-image" class="n-image" /> </div>
                                        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-9">
                                            <h5 class="clr-blu">
                                                12 Dec 2021</h5>
                                            <p> The present educaional and social structure does not help the </p>
                                        </div>
                                    </li>
                                    <li class="li-ele d-flex">
                                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3"> <img src="assets/images/n1.png" alt="news-image" class="n-image" /> </div>
                                        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-9">
                                            <h5 class="clr-blu">
                                                12 Dec 2021</h5>
                                            <p> The present educaional and social structure does not help the </p>
                                        </div>
                                    </li>
                                    <li class="li-ele d-flex">
                                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3"> <img src="assets/images/n1.png" alt="news-image" class="n-image" /> </div>
                                        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-9">
                                            <h5 class="clr-blu">
                                                12 Dec 2021</h5>
                                            <p> The present educaional and social structure does not help the </p>
                                        </div>
                                    </li>
                                    <li class="li-ele d-flex">
                                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3"> <img src="assets/images/n1.png" alt="news-image" class="n-image" /> </div>
                                        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-9">
                                            <h5 class="clr-blu">
                                                12 Jun 2021</h5>
                                            <p> The present educaional and social structure does not help the </p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12">
                        <div class="parent-voices">
                            <div class="p-t-20">
                                <h2 class="h2-title text-center clr-fff">
                                    Parent Voices</h2> </div>
                            <div class="slider single-item">
                                <div class="testmonial p-t-20">
                                    <h2 class="parent-tess">
                                        <img src="assets/images/no-photo.png" alt="nophoto" class="parentimage" />
                                        Akila Ponnusamy
                                    </h2>
                                    <p class="p-t-20 text-center clr-fff"> Striving to provide a well balanced spiritual, academic and social well-being, students are encouraged to both serve and participate in and beyond our community.</p>
                                </div>
                                <div class="testmonial">
                                    <h2 class="parent-tess p-t-20">
                                        <img src="assets/images/no-photo.png" alt="nophoto" class="parentimage" />
                                        Akila Ponnusamy
                                    </h2>
                                    <p class="p-t-20 text-center clr-fff"> Striving to provide a well balanced spiritual, academic and social well-being, students are encouraged to both serve and participate in and beyond our community.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal" id="modalPopup">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-head">
                <h2 class="subtitle text-left">
                    For Admissions</h2>
                <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
            </div>
            <div class="m-t-20">
                <label class="form-label"> Name</label>
                <input name="ctl00$ContentPlaceHolder1$txtName" type="text" id="ContentPlaceHolder1_txtName" class="form-control" /> </div>
            <div class="m-t-20">
                <label class="form-label"> Email</label>
                <input name="ctl00$ContentPlaceHolder1$TextBox1" type="text" id="ContentPlaceHolder1_TextBox1" class="form-control" /> </div>
            <div class="m-t-20">
                <label class="form-label"> Mobile Number</label>
                <input name="ctl00$ContentPlaceHolder1$TextBox2" type="text" id="ContentPlaceHolder1_TextBox2" class="form-control" /> </div>
            <div class="m-t-20 text-center">
                <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php';?>
</div>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/popper.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.js"></script>
<script type="text/javascript">
    $('#owl-demo').owlCarousel({
        rtl: false,
        loop: true,
        margin: 10,
        nav: true,
        autoplay: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    })
</script>
<script type="text/javascript" src="assets/js/venom-button.min.js"></script>
<script type="text/javascript">
    $('#myButton').venomButton({
        phone: '917755990720',
        chatMessage: 'Hello!, How Can we Help You',
        message: "",
        nameClient: "jQuery Script",
        showPopup: true
    });
</script>
<script type="text/javascript" src="assets/js/slick.js"></script>
<script type="text/javascript">
    $('.autoplay').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
    });
</script>
<script type="text/javascript">
    $('.single-item').slick();
</script>
<script type="text/javascript" src="assets/js/owl.carousel.js"></script>
<script type="text/javascript">
    var sync1 = $(".slider1");
    var sync2 = $(".navigation-thumbs");
    var thumbnailItemClass = '.owl-item';
    var slides = sync1.owlCarousel({
        video: true,
        startPosition: 0,
        items: 1,
        loop: false,
        margin: 0,
        autoplay: false,
        autoplayTimeout: 1000,
        autoplayHoverPause: false,
        nav: false,
        dots: false
    }).on('changed.owl.carousel', syncPosition);

    function syncPosition(el) {
        $owl_slider = $(this).data('owl.carousel');
        var loop = $owl_slider.options.loop;
        if(loop) {
            var count = el.item.count - 1;
            var current = Math.round(el.item.index - (el.item.count / 2) - .5);
            if(current < 0) {
                current = count;
            }
            if(current > count) {
                current = 0;
            }
        } else {
            var current = el.item.index;
        }
        var owl_thumbnail = sync2.data('owl.carousel');
        var itemClass = "." + owl_thumbnail.options.itemClass;
        var thumbnailCurrentItem = sync2.find(itemClass).removeClass("synced").eq(current);
        thumbnailCurrentItem.addClass('synced');
        if(!thumbnailCurrentItem.hasClass('active')) {
            var duration = 600;
            sync2.trigger('to.owl.carousel', [current, duration, true]);
        }
    }
    var thumbs = sync2.owlCarousel({
        startPosition: 1,
        items: 6,
        loop: false,
        margin: 0,
        autoplay: false,
        nav: false,
        dots: false,
        onInitialized: function(e) {
            var thumbnailCurrentItem = $(e.target).find(thumbnailItemClass).eq(this._current);
            thumbnailCurrentItem.addClass('synced');
        },
    }).on('mouseover', thumbnailItemClass, function(e) {
        e.preventDefault();
        var duration = 600;
        var itemIndex = $(e.target).parents(thumbnailItemClass).index();
        sync1.trigger('to.owl.carousel', [itemIndex, duration, true]);
    }).on("changed.owl.carousel", function(el) {
        var number = el.item.index;
        $owl_slider = sync1.data('owl.carousel');
        $owl_slider.to(number, 100, true);
    });
</script>
<script type="text/javascript" src="assets/js/jquery.easy-ticker.js"></script>
<script type="text/javascript">
    $(function() {
        $('.newstick').easyTicker({
            direction: 'up',
            visible: 3,
        });
    });
</script>
<script defer type="text/javascript" src="assets/js/jquery.flexslider-min.js"></script>
<script type="text/javascript">
    // Can also be used with $(document).ready()
    $(window).load(function() {
        // Vimeo API nonsense
        var player = document.getElementById('player_1');
        $f(player).addEvent('ready', ready);

        function addEvent(element, eventName, callback) {
            if(element.addEventListener) {
                element.addEventListener(eventName, callback, false)
            } else {
                element.attachEvent(eventName, callback, false);
            }
        }

        function ready(player_id) {
            var froogaloop = $f(player_id);
            froogaloop.addEvent('pause', function(data) {
                $('.flexslider').flexslider("play");
            });
            froogaloop.addEvent('play', function(data) {
                $('.flexslider').flexslider("play");
            });
        }
        // Call fitVid before FlexSlider initializes, so the proper initial height can be retrieved.
        $(".flexslider").fitVids().flexslider({
            animation: "slide",
            slideshowSpeed: 30000,
            animationSpeed: 1000,
            useCSS: false,
            animationLoop: true,
            smoothHeight: true,
            before: function(slider) {
                $f(player).api('play');
            }
        });
    });

    $(function() {

        $("input#enquery_name").keyup(function(){
            if ($(this).val()==''){
                $('input#enquery_name').css("border", "1px solid red");
            }else {
                $('input#enquery_name').css("border", "1px solid green");
            }
        });

        $("input#enquery_email").keyup(function(){
            if ($(this).val()==''){
                $('input#enquery_email').css("border", "1px solid red");
            }else {
                $('input#enquery_email').css("border", "1px solid green");
            }

            if (isValidEmailAddress($(this).val()) && $(this).val()!=''){
                $('input#enquery_email').css("border", "1px solid green");
                $('p#email_eror').html("");
            }

        });

        $("input#enquery_mobile_no").keyup(function(){
            if ($(this).val()==''){
                $('input#enquery_mobile_no').css("border", "1px solid red");
            }else {
                $('input#enquery_mobile_no').css("border", "1px solid green");
            }
        });

        $("input#enquery_invest_amount").keyup(function(){
            if ($(this).val()==''){
                $('input#enquery_invest_amount').css("border", "1px solid red");
            }else {
                $('input#enquery_invest_amount').css("border", "1px solid green");
            }
        });

        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test(emailAddress);
        }

        $('#enquery_form_submit').on('click', function(e){

            var baseUrl          ='<?=$baseUrl?>';
            var name             =$('input#enquery_name').val();
            var email            =$('input#enquery_email').val();
            var mobileno         =$('input#enquery_mobile_no').val();
            var investAmount     =$('input#enquery_invest_amount').val();

            var error=0;

            if (name==''){
                $('input#enquery_name').css("border", "1px solid red");
                error=1;
            }else {
                $('input#enquery_name').css("border", "1px solid green");
            }

            if (email==''){
                $('input#enquery_email').css("border", "1px solid red");
                error=1;
            }else {
                $('input#enquery_email').css("border", "1px solid green");
            }

            if (mobileno==''){
                $('input#enquery_mobile_no').css("border", "1px solid red");
                error=1;
            }else {
                $('input#enquery_mobile_no').css("border", "1px solid green");
            }

            if (investAmount==''){
                $('input#enquery_invest_amount').css("border", "1px solid red");
                error=1;
            }else {
                $('input#enquery_invest_amount').css("border", "1px solid green");
            }

            if (!isValidEmailAddress(email) && email!=''){
                $('input#enquery_email').css("border", "1px solid red");
                $('p#email_eror').html("Invalid email address");
            }

            if (error==1){
                return false;
            }

            if (error==0){
                $.ajax({
                    url:baseUrl+'/email/enquery_email.php',
                    type: "POST",
                    data: {
                        name  :name,
                        email: email,
                        mobileno: mobileno,
                        investAmount: investAmount,
                    },
                    success: function(data){
                        if (data==1){
                            $('form#enquiry_form_submit').html("<h2 class='Sucess-message'>Thank you for writing to us. Our representative will soon contact you and address your queries shortly</h2>");
                        }
                    },
                    error: function(){
                        alert("Something went wrong")
                    }
                });
            }
        });
    });

</script>
<!-- Sliders -->
<script type="text/javascript" src="assets/js/owl.carousel.js"></script>
<script type="text/javascript" src="assets/js/shBrushXml.js"></script>
<script type="text/javascript" src="assets/js/shBrushJScript.js"></script>
<!-- Syntax Highlighter -->
<script type="text/javascript" src="assets/js/shCore.js"></script>
<script type="text/javascript" src="assets/js/shBrushXml.js"></script>
<script type="text/javascript" src="assets/js/shBrushJScript.js"></script>
<!-- Optional FlexSlider Additions -->
<script type="text/javascript" src="assets/js/froogaloop.js"></script>
<script type="text/javascript" src="assets/js/jquery.fitvid.js"></script>
<script type="text/javascript" src="assets/js/demo.js"></script>
</body>

</html>