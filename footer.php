<div class="footer">
    <div class="p-t-100">
        <div class="container">
            <h2 class="h2-title text-center clr-fff position-relative">
                INDO-BRITISH GLOBAL SCHOOL</h2>
            <div class="p-t-50 p-b-50">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="d-flex1">
                            <p class="clr-fff f-icon"> <i class="fas fa-map-marker-alt"></i> </p>
                            <p class="clr-fff"> 170, Dnyaneshwar Colony,
                                <br /> Opp. N.D Tower's,
                                <br /> Behind PCMC Hospital,
                                <br /> Akurdi, Pune, 411035</p>
                        </div>
                        <div class="p-t-10 d-flex1">
                            <p class="clr-fff f-icon"> <i class="fas fa-phone-alt"></i> </p>
                            <p class="clr-fff"> +91 775599 0720 / 21 </p>
                        </div>
                        <div class="p-t-10 d-flex1">
                            <p class="clr-fff f-icon"> <i class="fas fa-envelope"></i> </p>
                            <p class="clr-fff"> enquiry@indo-british.com | partner@vaishvikshodh.com</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <ul class="footer-list">
                            <li><i class="fas fa-angle-double-right"></i><a href="index.php">Home</a> </li>
                            <li><i class="fas fa-angle-double-right"></i><a href="about.php">About Us</a> </li>
                            <li><i class="fas fa-angle-double-right"></i><a href="programs.php">Programs</a> </li>
                            <li><i class="fas fa-angle-double-right"></i><a href="faq.php">FAQ</a> </li>
                            <li><i class="fas fa-angle-double-right"></i><a href="#">Terms & Conditions</a> </li>
                        </ul>
                        <div class="col-lg-12">
                            <div class="p-t-10">
                                <ul class="s-icons">
                                    <li><a href="https://www.facebook.com/IndoBritishGlobal" class="f-bg clr-fff"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="https://twitter.com/IndoBritishGS" class="t-bg clr-fff"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="https://www.youtube.com/channel/UChWaOGQYRcA7dYd2kOqjM2w" class="y-bg clr-fff"><i class="fab fa-youtube"></i></a></li>
                                    <li><a href="https://www.instagram.com/indo.british.global.school/" class="i-bg clr-fff"><i class="fab fa-instagram"></i></a></li>
                                    <li><a href="https://www.linkedin.com/company/indo-british-global-school" class="f-bg clr-fff"><i class="fab fa-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="p-t-10"> <img src="assets/images/logo.jpg" alt="logo-unicorn" width="100%" /> </div>
                        <div class="p-t-10"> <img src="assets/images/unicorn.png" alt="logo-unicorn" width="100%" style="background: #fff;
                                        padding: 5px 15px" /> </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="p-t-10"> <img src="assets/images/logot.jpg" alt="logo-unicorn" width="100%" /> </div>
                        <div class="p-t-10"> <img src="assets/images/logow.jpg" alt="logo-unicorn" width="100%" /> </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right">
            <p> © 2021 Indo-British Global School.Powered By <a href="https://innovasphere.com/">Innovasphere.com</a></p>
        </div>
    </div>
</div>