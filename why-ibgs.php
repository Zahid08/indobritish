<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <title> INDO BRITISH GLOBAL SCHOOL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" />
    <meta charset="utf-8" />
    <meta name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png" />
    <link rel="manifest" href="assets/images/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="ms-icon-144x144.php" />
    <meta name="theme-color" content="#ffffff" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/slick.css" />
    <link rel="stylesheet" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css" />
    <link rel="stylesheet" href="assets/css/venom-button.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/notify-bootstrap.css" />
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css" />
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
</head>

<body style="background: none">
<form method="post" action="https://indo-british.com/why-ibgs.aspx" id="form1">
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTY1NDU2MTA1MmRkfa3RDYynd78+KRM+w5U5oKh7qDQTXqm0Io0U5Jo+H4E=" /> </div>
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E21F4C83" />
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAQuf997CIDKSQGTNkLS+BTzt0GqqGmTunmU8BFOEDL5JLt0vKVmDtQYb7KLGs2qWD1/2i7OlKCH0kBh9oGA4EXW8TTULyQBK15BrWTlFLlZrbrN6i6P3BGue6QqPFpZgII=" /> </div>
    <div>
        <?php include 'header.php';?>
        <div id="myButton"></div>
    </div>
    <div class="page-banner"> <img src="assets/images/sub1.jpg" alt="sub-banner" width="100%" class="sub-banner" />
        <h1 class="h1-banner text-uppercase">
            why indo-british Global school?</h1> </div>
    <div class="page">
        <div class="container">
            <div class="m-t-100 m-b-100">
                <div class="wow fadeInUp">
                    <p> IBGS is a first in its domain who believes that every child in the has unique potential and the schools should provide them the necessary guidelines and teaching learning approach to make them happy, future ready global citizens. We at IBGS has created the global integrated curricula with unique teaching learning approach “Ecoliere Infinity Learning Approach”. </p>
                    <ul class="list-none m-t-30">
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Proven and industry best team to support.</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Assured operational breakeven in the first year of school operation*</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Unique architectural and interior for the school (BALA Concept)</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Unique and tested student resource and teacher resource and interactive digital content</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Continuous updates on the global educational developments</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Dedicated support for infrastructure, PMC, Operationalisation, Marketing and academics.</p>
                        </li>
                        <li class="align-baseline"><i class="fas fa-angle-double-right"></i>
                            <p> Reduced set up cost (World class architect with enhanced usable area)</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div>
            <div class="gray-bg wow fadeInDown">
                <div class="container p-b-50">
                    <h2 class="subtitle text-center p-t-50">
                        Six Reasons That Makes Us A<span> Dream Partner</span>
                    </h2>
                    <div class="p-t-50">
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="box-modal">
                                    <div class="r-image"> <img src="assets/images/r1.png" alt="r1-image" /> </div>
                                    <p> India’s first Chain of school with integrated global curriculam </p>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="box-modal">
                                    <div class="r-image"> <img src="assets/images/r2.png" alt="r1-image" /> </div>
                                    <p> On Ground Marketing support to ensure the best in class IRR </p>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="box-modal">
                                    <div class="r-image"> <img src="assets/images/r3.png" alt="r1-image" /> </div>
                                    <p> Proprietary pedagogy to ensure infinite learning for every child </p>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="box-modal">
                                    <div class="r-image"> <img src="assets/images/r4.png" alt="r1-image" /> </div>
                                    <p> Technology enabled content for experiential Learning </p>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="box-modal">
                                    <div class="r-image"> <img src="assets/images/r5.png" alt="r1-image" /> </div>
                                    <p> User friendly ERP to take care of every stakeholders need </p>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="box-modal">
                                    <div class="r-image"> <img src="assets/images/r6.png" alt="r1-image" /> </div>
                                    <p> Unique Infrastructure planning and execution by world class architect </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'footer.php';?>
    </div>
    </div>
    <div class="modal" id="modalPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    <h2 class="subtitle text-left">
                        For Admissions</h2>
                    <button type="button" class="close text-right" data-dismiss="modal" aria-hidden="true"> &times;</button>
                </div>
                <div class="m-t-20">
                    <label class="form-label"> Name</label>
                    <input name="ctl00$txtName" type="text" id="txtName" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Email</label>
                    <input name="ctl00$TextBox1" type="text" id="TextBox1" class="form-control" /> </div>
                <div class="m-t-20">
                    <label class="form-label"> Mobile Number</label>
                    <input name="ctl00$TextBox2" type="text" id="TextBox2" class="form-control" /> </div>
                <div class="m-t-20 text-center">
                    <button type="button" class="btn send-btn" data-dismiss="modal"> Send</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include 'footer-scripts.php';?>
</body>

</html>